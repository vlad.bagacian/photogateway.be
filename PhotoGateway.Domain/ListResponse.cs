﻿using System.Collections;
using System.Collections.Generic;

namespace PhotoGateway.Domain
{
    public class ListResponse<T>
    {
        public ListResponse(List<T> items)
        {
            Items = items;
            Count = items.Count;
        }

        public List<T> Items { get; set; }

        public int Count { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using PhotoGateway.Domain.UserModel;

namespace PhotoGateway.Domain.UserModel.Request
{
    public class UpdateUserRequest
    { 

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Phone]
        public string Phone { get; set; }

        [Required]
        public Address Address { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using PhotoGateway.Domain.UserModel;

namespace PhotoGateway.Domain.UserModel.Request
{
    public class UserRequest
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MinLength(6)]
        public string Password { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Phone]
        public string Phone { get; set; }

        [Required]
        public UserRole Role { get; set; }

        [Required]
        public Address Address { get; set; }
    }
}

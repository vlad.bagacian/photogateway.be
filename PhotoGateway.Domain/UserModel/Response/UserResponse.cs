﻿using System.Collections.Generic;

namespace PhotoGateway.Domain.UserModel.Response
{
    public class UserResponse
    {
        public string Id { get; set; }

        public string EId { get; set; }

        public string Auth0Id { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public bool EmailVerified { get; set; }

        public Address Address { get; set; }

        public string ProfileImageUrl { get; set; }

        public UserRole Role { get; set; }
    }
}

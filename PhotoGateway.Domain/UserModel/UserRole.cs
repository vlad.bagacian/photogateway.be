﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PhotoGateway.Domain.UserModel
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum UserRole
    {
        Client,
        Photographer
    }
}
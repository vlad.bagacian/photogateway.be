﻿using System.ComponentModel.DataAnnotations;

namespace PhotoGateway.Domain.UserModel
{
    public class Address
    {
        [Required]
        public string Street { get; set; }

        [Required]
        public string City { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }

        public string Country { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace PhotoGateway.Domain.Session
{
    public class DashboardStatistics
    {
        public List<SessionStatistics> SessionStatistics { get; set; }

        public long NewProjects { get; set; }

        public long ProjectsInProgress  { get; set; }

        public long Completed { get; set; }

        public long TotalUsers { get; set; }
    }
}

﻿using System;

namespace PhotoGateway.Domain.Session.Request
{
    public class CreateSessionRequest
    {
        public string PhotographerId { get; set; }

        public DateTime PhotoShootStartDate { get; set; }

        public DateTime PhotoShootEndDate { get; set; }

        public DateTime DueAt { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Location { get; set; }

        public int PhotoCount { get; set; }
    }
}

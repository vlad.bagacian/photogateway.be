﻿using System;

namespace PhotoGateway.Domain.Session.Request
{
    public class UpdateSessionRequest
    {
        public string Title { get; set; }   

        public string Description { get; set; }

        public decimal? Cost { get; set; }

        public DateTime? DueAt { get; set; }
    }
}

﻿namespace PhotoGateway.Domain.Session.Request
{
    public class ListSessionRequest : ListRequest
    {
        public SessionStatus? SessionStatus { get; set; }
    }
}

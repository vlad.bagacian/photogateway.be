﻿namespace PhotoGateway.Domain.Session.Request
{
    public class ChangeStatusRequest
    {
        public SessionStatus SessionStatus { get; set; }
    }
}

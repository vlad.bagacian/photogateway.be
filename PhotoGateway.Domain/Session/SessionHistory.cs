﻿using System;

namespace PhotoGateway.Domain.Session
{
    public class SessionHistory
    {
        public DateTime DateTime { get; set; }

        public SessionStatus Status { get; set; }
    }
}

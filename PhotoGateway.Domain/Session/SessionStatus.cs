﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PhotoGateway.Domain.Session
{
    [JsonConverter(typeof(StringEnumConverter))]    
    public enum SessionStatus
    {
        New,
        Cancelled,
        ProjectReview,
        Approval,
        InProgress,
        InReview,
        Completed
    }
}

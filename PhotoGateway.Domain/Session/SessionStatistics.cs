﻿namespace PhotoGateway.Domain.Session
{
    public class SessionStatistics
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int SessionCount { get; set; }
        public decimal TotalCost { get; set; }
    }
}

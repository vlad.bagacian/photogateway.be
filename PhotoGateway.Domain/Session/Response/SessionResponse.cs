﻿using System;
using System.Collections.Generic;

namespace PhotoGateway.Domain.Session.Response
{
    public class SessionResponse : ObjectResponse
    {
        public DateTime? StartedAt { get; set; }

        public DateTime CreatedAt { get; set; }

        public string PhotographerId { get; set; }

        public string PhotographerName { get; set; }

        public string PhotographerAvatar { get; set; }

        public string PhotographerEmail { get; set; }

        public string ClientId { get; set; }

        public string ClientName { get; set; }

        public string ClientEmail { get; set; }

        public string ClientAvatar { get; set; }

        public DateTime DueAt { get; set; }

        public DateTime PhotoShootStartDate { get; set; }

        public DateTime PhotoShootEndDate { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Location { get; set; }

        public decimal? Cost { get; set; }

        public SessionStatus Status { get; set; }

        public List<SessionHistory> SessionHistory { get; set; }
    }
}

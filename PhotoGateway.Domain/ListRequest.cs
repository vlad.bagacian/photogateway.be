﻿namespace PhotoGateway.Domain
{
    public class ListRequest
    {
        private int _top = 100;
        private int _skip = 0;

        public int Top
        {
            get { return _top; }
            set { _top = value > 100 || value <= 0 ? 100 : value; }
        }

        public int Skip
        {
            get { return _skip; }
            set { _skip = value < 0 ? 0 : value; }
        }
    }
}

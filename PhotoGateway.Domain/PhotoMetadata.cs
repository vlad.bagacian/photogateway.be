﻿using System.Collections.Generic;

namespace PhotoGateway.Domain
{
    public class PhotoMetadata
    {
        public string ImageUrl { get; set; }

        public decimal Ratio { get; set; }

        public List<string> History { get; set; }
    }
}

﻿using PhotoGateway.Domain.Photographer;
using System;

namespace PhotoGateway.Domain
{
    public class PhotographyResponse : ObjectResponse
    {
        public DateTime CreatedAt { get; set; }

        public PhotoMetadata Metadata { get; set; }

        public string CameraMake { get; set; }

        public string CameraModel { get; set; }

        public ExposureSettings ExposureSettings { get; set; }

        public bool Selected { get; set; }
    }
}

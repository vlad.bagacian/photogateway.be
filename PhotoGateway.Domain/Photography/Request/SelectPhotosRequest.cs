﻿using System.Collections.Generic;

namespace PhotoGateway.Domain.Photography.Request
{
    public class SelectPhotosRequest
    {
        public List<string> PhotoIds { get; set; }
    }
}

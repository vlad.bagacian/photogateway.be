﻿namespace PhotoGateway.Domain.App
{
    public class Auth0Settings
    {
        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string ClientDomain { get; set; }

        public string Audience { get; set; }
    }
}

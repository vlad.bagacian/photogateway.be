﻿namespace PhotoGateway.Domain.App
{
    public static class Auth0Configuration
    {
        public static string AuthenticationScheme = "Auth0";

        public static string ClientId = "Auth0Settings:ClientId";

        public static string ClientSecret = "Auth0Settings:ClientSecret";

        public static string ClientDomain = "Auth0Settings:ClientDomain";

        public static string Audience = "Auth0Settings:Audience";
    }
}

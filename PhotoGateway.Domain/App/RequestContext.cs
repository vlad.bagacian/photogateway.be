﻿using Microsoft.AspNetCore.Http;
using PhotoGateway.Domain.UserModel;
using PhotoGateway.Domain.UserModel.Response;

namespace PhotoGateway.Domain.App
{
    public class RequestContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public RequestContext(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            if (_user == null && _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated)
            {
                _user = (UserResponse)_httpContextAccessor.HttpContext.Items["User"];
            }
        }

        private UserResponse _user;

        public UserResponse User
        {
            get
            {
                if (_user == null && _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated)
                {
                    _user = (UserResponse)_httpContextAccessor.HttpContext.Items["User"];
                }

                return _user;
            }
        }

        public string UserId
        {
            get
            {
                return _user.EId;
            }
        }

        public string AccountId
        {
            get
            {
                return _user.Id;
            }
        }

        public UserRole UserRole
        {
            get
            {
                return _user.Role;
            }
        }

        public string UserName
        {
            get
            {
                return $"{_user.FirstName} {_user.LastName}";
            }
        }
    }
}

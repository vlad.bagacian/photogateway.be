﻿namespace PhotoGateway.Domain.App
{
    public class AzureSettings
    {
        public string ConnectionString { get; set; }

        public string BlobContainerName { get; set; }
    }
}

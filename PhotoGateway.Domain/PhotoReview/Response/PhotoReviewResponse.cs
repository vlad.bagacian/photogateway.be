﻿using System;

namespace PhotoGateway.Domain.PhotoReview.Response
{
    public class PhotoReviewResponse : ObjectResponse
    {
        public string UserName { get; set; }

        public string Comment { get; set; }

        public DateTime CreatedAt { get; set; }

        public string ImageUrl { get; set; }

        public bool Sent { get; set; }
    }
}

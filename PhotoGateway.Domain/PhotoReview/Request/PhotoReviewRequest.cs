﻿namespace PhotoGateway.Domain
{
    public class AddCommentRequest
    {
        public string Comment { get; set; }
    }
}

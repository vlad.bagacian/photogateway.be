﻿namespace PhotoGateway.Domain
{
    public class UploadedFile
    {
        public string Name { get; set; }

        public PhotoMetadata Metadata { get; set; }

        public string Thumbnail { get; set; }

        public string UserId { get; set; }

        public string SessionId { get; set; }
    }
}

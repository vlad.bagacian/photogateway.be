﻿using System.Net;

namespace PhotoGateway.Domain.Exceptions
{
    public class NotFoundException : ServiceException
    {
        public NotFoundException(string message, string errorCode) : base(message, errorCode)
        {
            this.StatusCode = HttpStatusCode.NotFound;
        }
    }
}

﻿using System.Net;
using System;

namespace PhotoGateway.Domain.Exceptions
{
    public class ServiceException : Exception
    {
        public string ErrorCode { get; set; }

        public string Message { get; set; }

        public HttpStatusCode StatusCode { get; protected set; }

        public ServiceException(string message, string errorCode) : base(message)
        {
            this.Message = message;
            this.ErrorCode = errorCode;
        }
    }
}

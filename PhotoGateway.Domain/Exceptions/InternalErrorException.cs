﻿using System.Net;

namespace PhotoGateway.Domain.Exceptions
{
    public class InternalErrorException : ServiceException
    {
        public InternalErrorException(string message, string errorCode) : base(message, errorCode)
        {
            this.StatusCode = HttpStatusCode.InternalServerError;
        }
    }
}

﻿namespace PhotoGateway.Domain
{
    public abstract class ObjectResponse
    {
        public string Id { get; set; }
    }
}

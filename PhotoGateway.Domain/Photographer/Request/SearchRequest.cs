﻿using System;
using System.Text.Json.Serialization;

namespace PhotoGateway.Domain.Photographer.Request
{
    public class SearchRequest
    {
        [JsonPropertyName("location")]
        public string Location { get; set; }

        [JsonPropertyName("category")]
        public PhotographySkill Category { get; set; }

        [JsonPropertyName("date")]
        public DateTime Date { get; set; }
    }
}

﻿using System;

namespace PhotoGateway.Domain.Photographer.Request
{
    public class ListPhotographersRequest : ListRequest
    {
        public string Location { get; set; }

        public PhotographySkill Category { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public decimal CostStartRange { get; set; } = 0;

        public decimal CostEndRange { get; set; } = 5000;

        public string SortBy { get; set; }

        public bool SortDescending { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace PhotoGateway.Domain.Photographer.Request
{
    public class PhotographerUpdateRequest
    {
        public string Biography { get; set; }

        public List<PhotographySkill> Skills { get; set; }

        public decimal Cost { get; set; }

        public string FacebookProfile { get; set; }

        public string InstagramProfile { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace PhotoGateway.Domain.Photographer.Response
{
    public class PhotographerResponse : ObjectResponse
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Biography { get; set; }

        public decimal Rating { get; set; }

        public decimal Cost { get; set; }

        public string ProfileImageUrl { get; set; }

        public List<PhotoMetadata> PortfolioPhotos { get; set; }

        public List<PhotographySkill> Skills { get; set; }

        public string FacebookProfile { get; set; }

        public string InstagramProfile { get; set; }
    }
}

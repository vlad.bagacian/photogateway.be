﻿using PhotoGateway.Domain.UserModel.Response;
using System.Collections.Generic;

namespace PhotoGateway.Domain.Photographer.Response
{
    public class Photographer
    {
        public string Id { get; set; }

        public string UserId { get; set; }

        public string Biography { get; set; }

        public List<PhotographySkill> Skills { get; set; }

        public bool IsProfileCompleted { get; set; }

        public string Location { get; set; }

        public string FacebookProfile { get; set; }

        public string InstagramProfile { get; set; }
    }
}

﻿using PhotoGateway.Domain.UserModel;
using System.Collections.Generic;

namespace PhotoGateway.Domain.Photographer.Response
{
    public class PhotographerProfile : Profile
    {
        public string Id { get; set; }

        public string Biography { get; set; }

        public List<PhotographySkill> Skills { get; set; }

        public bool IsProfileCompleted { get; set; }

        public decimal Cost { get; set; }

        public decimal Rating { get; set; }

        public List<PhotoMetadata> PortfolioPhotos { get; set; }

        public string FacebookProfile { get; set; }

        public string InstagramProfile { get; set; }
    }
}

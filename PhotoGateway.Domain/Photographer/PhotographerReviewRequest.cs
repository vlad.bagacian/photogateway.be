﻿namespace PhotoGateway.Domain.Photographer
{
    public class PhotographerReviewRequest
    {
        public decimal Rating { get; set; }

        public string Comment { get; set; }
    }
}

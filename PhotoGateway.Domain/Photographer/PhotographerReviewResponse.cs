﻿using System;

namespace PhotoGateway.Domain.Photographer
{
    public class PhotographerReviewResponse
    {
        public decimal Rating { get; set; }

        public string Comment { get; set; }

        public DateTime CreatedAt { get; set; }

        public string ClientName { get; set; }
    }
}

﻿namespace PhotoGateway.Domain.Photographer
{
    public class CompleteProfileRequest
    {
        public string Biography { get; set; }

        public PhotographySkill Skills { get; set; }
    }
}
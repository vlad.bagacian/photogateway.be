﻿namespace PhotoGateway.Domain.Photographer
{
    public class ExposureSettings
    {
        public string ShutterSpeed { get; set; }

        public string Aperture { get; set; }

        public string Iso { get; set; }
    }
}

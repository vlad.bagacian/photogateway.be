﻿

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PhotoGateway.Domain.Photographer
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum PhotographySkill
    {
        None,
        Landscape,
        Portrait,
        Wedding,
        Fashion,
        Event,
        Sports,
        Aerial,
        Macro,
        Underwater,
        Wildlife
        // add more skills as necessary
    }
}

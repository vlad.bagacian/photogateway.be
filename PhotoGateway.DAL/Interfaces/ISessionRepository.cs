﻿using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.Session;
using PhotoGateway.Domain.Session.Request;
using PhotoGateway.Domain.UserModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Interfaces
{
    public interface ISessionRepository<T> : IRepository<T>
    {
        Task<List<SessionWithUsersEntity>> GetAll(string userId, ListSessionRequest request);

        Task<SessionWithUsersEntity> GetById(string id, string userId);
        Task<DashboardStatistics> GetDashboardStatsAsync(string userId, UserRole role);
        Task<IEnumerable<SessionStatistics>> GetSessionStatistics(string photographerId, int year);
    }
}

﻿using PhotoGateway.DAL.Models;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Interfaces
{
    public interface IUserRepository<T> : IRepository<T>
    {
        Task<UserEntity> GetByAuth0IdAsync(string auth0Id);
    }
}

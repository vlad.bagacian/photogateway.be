﻿using PhotoGateway.DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Interfaces
{
    public interface IPhotographerReviewRepository<T> : IRepository<T>
    {
        Task<List<PhotographerReviewEntityExtended>> GetByPhotographerId(string photographerId);
    }
}

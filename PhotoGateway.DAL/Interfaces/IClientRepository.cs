﻿using PhotoGateway.DAL.Models;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Interfaces
{
    public interface IClientRepository<T> : IRepository<T>
    {
        Task<ClientExtendedEntity> GetByClientId(string clientId);
        Task<ClientEntity> GetByUserId(string userId);
    }
}

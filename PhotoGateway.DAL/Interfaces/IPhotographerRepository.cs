﻿using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.Photographer;
using PhotoGateway.Domain.Photographer.Request;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Interfaces
{
    public interface IPhotographerRepository<T> : IRepository<T>
    {
        Task<List<PhotographerExtendedEntity>> GetAllFilteredAsync(ListPhotographersRequest request);
        Task<PhotographerExtendedEntity> GetByPhotographerId(string photographerId);
        Task<PhotographerExtendedEntity> GetByUserId(string userId);
    }
}

﻿using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.PhotoReview.Request;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Interfaces
{
    public interface IPhotoReviewRepository<T> : IRepository<T>
    {
        Task<List<T>> GetByPhotoId(string id);
        Task<List<ReviewExtendedEntity>> GetByPhotoId(string photoId, ListReviewsRequest request);
    }
}

﻿using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.Photography.Request;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Interfaces
{
    public interface IPhotographyRepository<T> : IRepository<T>
    {
        Task AddManyAsync(List<PhotographyEntity> photograpies);
        Task<List<T>> GetAll(string sessionId);
        Task<List<PhotographyEntity>> GetAllAsync(string sessionId, ListPhotosRequest request = null);
        Task<List<PhotographyEntity>> GetAllByIds(HashSet<string> ids);
    }
}

﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Interfaces
{
    public interface IRepository<T>
    {
        Task AddAsync(T entity);

        Task DeleteAsync(string id);

        Task<T> GetById(string id);

        Task<List<T>> GetAllByIds(HashSet<string> ids);

        Task InsertManyAsync(List<T> entities);

        Task<bool> IsConnected(CancellationToken cancellationToken = default);

        Task UpdateAsync(T entity);

        Task<string> UpdateManyAsync(IEnumerable<T> entities);
    }
}

﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using PhotoGateway.DAL.Interfaces;
using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.Photographer;
using PhotoGateway.Domain.Photographer.Request;
using PhotoGateway.Domain.Photographer.Response;
using PhotoGateway.Domain.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Repositories
{
    public class PhotographerRepository : MongoRepository<PhotographerEntity>, IPhotographerRepository<PhotographerEntity>
    {
        private const string CollectionName = "Photographers";

        public PhotographerRepository(IOptions<MongoDbSettings> settings) : base(settings, CollectionName)
        {
            CreateIndexes();
        }

        public async Task<PhotographerExtendedEntity> GetByUserId(string userId)
        {
            var userCollection = Database.GetCollection<UserEntity>("Users");
            var pipeline = this.collection.Aggregate()
                .Match(s => s.UserId == userId)
                .Lookup(
                    foreignCollection: userCollection,
                    localField: s => s.UserId,
                    foreignField: u => u.Id,
                    @as: (PhotographerExtendedEntity s) => s.User
                )
                .Unwind<PhotographerExtendedEntity, PhotographerExtendedEntity>(s => s.User)
                .As<PhotographerExtendedEntity>();

            var result = await pipeline.SingleOrDefaultAsync();
            return result;
        }

        public async Task<PhotographerExtendedEntity> GetByPhotographerId(string photographerId)
        {
            var userCollection = Database.GetCollection<UserEntity>("Users");

            var pipeline = this.collection.Aggregate()
                .Match(s => s.Id == photographerId)
                .Lookup(
                    foreignCollection: userCollection,
                    localField: s => s.UserId,
                    foreignField: u => u.Id,
                    @as: (PhotographerExtendedEntity s) => s.User
                )
                .Unwind<PhotographerExtendedEntity, PhotographerExtendedEntity>(s => s.User)
                .As<PhotographerExtendedEntity>();

            var result = await pipeline.SingleOrDefaultAsync();
            return result;
        }

        public async Task<List<PhotographerExtendedEntity>> GetAllFilteredAsync(ListPhotographersRequest request)
        {
            IMongoCollection<SessionEntity> sessions = Database.GetCollection<SessionEntity>("Sessions");
            IMongoCollection<UserEntity> userCollection = Database.GetCollection<UserEntity>("Users");


            SessionStatus[] includedStatuses = new[] { SessionStatus.New, SessionStatus.Cancelled, SessionStatus.Completed };

            Expression<Func<PhotographerWithSessions, bool>> filterByStatusAndDate = pws =>
                pws.Session.All(
                    s => includedStatuses.Contains(s.Status) ||
                    s.PhotoShootEndDate <= request.EndDate &&
                    s.PhotoShootEndDate <= request.StartDate ||
                    (s.PhotoShootStartDate >= request.EndDate && s.PhotoShootStartDate >= request.StartDate)
                );

            Expression<Func<PhotographerWithSessions, bool>> filterOnlyByStatus = pws => pws.Session.All(s => includedStatuses.Contains(s.Status));
            Expression<Func<PhotographerWithSessions, bool>> sessionStatusFilter = filterByStatusAndDate;
            if (request.StartDate == DateTime.MinValue || request.EndDate == DateTime.MinValue)
            {
                sessionStatusFilter = filterOnlyByStatus;
            }

            FilterDefinition<PhotographerWithSessions> statusFilter = Builders<PhotographerWithSessions>.Filter.Where(sessionStatusFilter);

            FilterDefinition<PhotographerWithSessions> sessionFilter = Builders<PhotographerWithSessions>.Filter.Or(
                Builders<PhotographerWithSessions>.Filter.Where(pws => !pws.Session.Any()),
                statusFilter);

            Expression<Func<PhotographerWithSessions, object>> sortBy = d => d.CreatedAt;
            if (request.SortBy?.Equals("cost", StringComparison.CurrentCultureIgnoreCase) ?? false)
            {
                sortBy = d => d.Cost;
            }
            else if (request.SortBy?.Equals("rating", StringComparison.CurrentCultureIgnoreCase) ?? false)
            {
                sortBy = d => d.Rating;
                request.SortDescending = true;
            }

            SortDefinition<PhotographerWithSessions> sortField = request.SortDescending ? Builders<PhotographerWithSessions>.Sort.Descending(sortBy) : Builders<PhotographerWithSessions>.Sort.Ascending(sortBy);

            List<FilterDefinition< PhotographerExtendedEntity>> filterDefinitions = new List<FilterDefinition< PhotographerExtendedEntity>>();
            filterDefinitions.Add(Builders<PhotographerExtendedEntity>.Filter.Eq(u => u.IsProfileCompleted, true));

            if (!string.IsNullOrEmpty(request.Location))
            {
                filterDefinitions.Add(Builders<PhotographerExtendedEntity>.Filter.Eq(u => u.Location, request.Location));
            }

            if (request.Category != PhotographySkill.None)
            {
                filterDefinitions.Add(Builders<PhotographerExtendedEntity>.Filter.AnyEq(x => x.Skills, request.Category));
            }

            FilterDefinition<PhotographerEntity> costFilter = Builders<PhotographerEntity>.Filter.And(
                    Builders<PhotographerEntity>.Filter.Gte(p => p.Cost, request.CostStartRange),
                    Builders<PhotographerEntity>.Filter.Lte(p => p.Cost, request.CostEndRange)
                );

            FilterDefinition<PhotographerEntity> photographersFilter = Builders<PhotographerEntity>.Filter.And(
                    Builders<PhotographerEntity>.Filter.Eq(u => u.IsProfileCompleted, true),
                    Builders<PhotographerEntity>.Filter.Eq(u => u.Location, request.Location),
                    Builders<PhotographerEntity>.Filter.AnyEq(x => x.Skills, request.Category),
                    costFilter
                );

            List<PhotographerWithSessions> result = await this.collection.Aggregate()
                .Match(photographersFilter)
                .Lookup(
                    foreignCollection: userCollection,
                    localField: s => s.UserId,
                    foreignField: u => u.Id,
                    @as: (PhotographerExtendedEntity s) => s.User)
                .Unwind<PhotographerExtendedEntity, PhotographerExtendedEntity>(s => s.User)
                .As<PhotographerExtendedEntity>()
                .Match(Builders<PhotographerExtendedEntity>.Filter.And(filterDefinitions))
                .Lookup<PhotographerExtendedEntity, SessionEntity, PhotographerWithSessions>(
                    sessions,
                    p => p.Id,
                    s => s.PhotographerId,
                    pws => pws.Session)
                .Match(sessionFilter)
                .Sort(sortField)
                .Skip(request.Skip)
                .Limit(request.Top)
                .ToListAsync();

            List<PhotographerExtendedEntity> filteredPhotographers = result.Select(r => new PhotographerExtendedEntity
            {
                Id = r.Id,
                CreatedAt = r.CreatedAt,
                ModifiedAt = r.ModifiedAt,
                UserId = r.UserId,
                Biography = r.Biography,
                Skills = r.Skills,
                PortfolioPhotos = r.PortfolioPhotos,
                Location = r.Location,
                IsProfileCompleted = r.IsProfileCompleted,
                Cost = r.Cost,
                Rating = r.Rating,
                User = r.User
            }).ToList();

            return filteredPhotographers;
        }

        private void CreateIndexes()
        {
            var photographersCollection = Database.GetCollection<PhotographerEntity>(CollectionName);

            var userIdIndex = Builders<PhotographerEntity>.IndexKeys.Ascending(x => x.UserId);

            var locationIndex = Builders<PhotographerEntity>.IndexKeys.Ascending(x => x.Location);
            var skillsIndex = Builders<PhotographerEntity>.IndexKeys.Ascending(x => x.Skills);
            var costIndex = Builders<PhotographerEntity>.IndexKeys.Ascending(x => x.Cost);
            var ratingIndex = Builders<PhotographerEntity>.IndexKeys.Ascending(x => x.Rating);

            var userIdIndexModel = new CreateIndexModel<PhotographerEntity>(userIdIndex);
            var locationIndexModel = new CreateIndexModel<PhotographerEntity>(locationIndex);
            var skillsIndexModel = new CreateIndexModel<PhotographerEntity>(skillsIndex);
            var costIndexModel = new CreateIndexModel<PhotographerEntity>(costIndex);
            var ratingIndexModel = new CreateIndexModel<PhotographerEntity>(ratingIndex);

            photographersCollection.Indexes.CreateMany(new[]
            {
                userIdIndexModel,
                locationIndexModel,
                skillsIndexModel,
                costIndexModel,
                ratingIndexModel
            });
        }

    }

    public class PhotographerWithSessions : PhotographerExtendedEntity
    {
        public List<SessionEntity> Session { get; set; }
    }
}

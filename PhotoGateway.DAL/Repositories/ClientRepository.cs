﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using PhotoGateway.DAL.Interfaces;
using PhotoGateway.DAL.Models;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Repositories
{
    public class ClientRepository : MongoRepository<ClientEntity>, IClientRepository<ClientEntity>
    {
        private const string CollectionName = "Clients";

        public ClientRepository(IOptions<MongoDbSettings> settings) : base(settings, CollectionName) { }

        public async Task<ClientEntity> GetByUserId(string userId)
        {
            var filter = Builders<ClientEntity>.Filter.Eq(u => u.UserId, userId);
            return await this.collection.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<ClientExtendedEntity> GetByClientId(string clientId)
        {
            var userCollection = Database.GetCollection<UserEntity>("Users");

            var pipeline = this.collection.Aggregate()
                .Match(s => s.Id == clientId)
                .Lookup(
                    foreignCollection: userCollection,
                    localField: s => s.UserId,
                    foreignField: u => u.Id,
                    @as: (ClientExtendedEntity s) => s.User
                )
                .Unwind<ClientExtendedEntity, ClientExtendedEntity>(s => s.User)
                .As<ClientExtendedEntity>();

            var result = await pipeline.SingleOrDefaultAsync();
            return result;
        }
    }
}
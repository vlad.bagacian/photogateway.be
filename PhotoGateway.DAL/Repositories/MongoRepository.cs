﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using PhotoGateway.DAL.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Repositories
{
    public abstract class MongoRepository<T> where T : BaseEntity
    {
        protected IMongoDatabase Database;

        protected readonly IMongoCollection<T> collection;

        protected MongoRepository(){}

        protected MongoRepository(IOptions<MongoDbSettings> settings, string collectionName)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            Database = client.GetDatabase(settings.Value.DatabaseName);
            this.collection = Database.GetCollection<T>(collectionName);
        }

        public async Task<T> GetById(string id)
        {
            var filter = Builders<T>.Filter.Eq(u => u.Id, id);
            return await this.collection.Find(filter).FirstOrDefaultAsync();
        }

        public async Task AddAsync(T entity)
        {
            await this.collection.InsertOneAsync(entity);
        }

        public async Task InsertManyAsync(List<T> entities)
        {
            await this.collection.InsertManyAsync(entities);
        }

        public async Task UpdateAsync(T entity)
        {
            var filter = Builders<T>.Filter.Eq(u => u.Id, entity.Id);
            await this.collection.ReplaceOneAsync(filter, entity);
        }

        public async Task<string> UpdateManyAsync(IEnumerable<T> entities)
        {
            var updates = new List<WriteModel<T>>();
            var filterBuilder = Builders<T>.Filter;

            foreach (var doc in entities)
            {
                var filter = filterBuilder.Eq(u => u.Id, doc.Id);
                updates.Add(new ReplaceOneModel<T>(filter, doc));
            }

            BulkWriteResult result = await this.collection.BulkWriteAsync(updates);
            return result.ModifiedCount.ToString();
        }

        public async Task DeleteAsync(string id)
        {
            var filter = Builders<T>.Filter.Eq(u => u.Id, id);
            await this.collection.DeleteOneAsync(filter);
        }

        public async Task<List<T>> GetAllByIds(HashSet<string> ids)
        {
            try
            {
                var filter = Builders<T>.Filter.In(e => e.Id, ids);
                var result = await collection.Find(filter).ToListAsync();

                return result;
            }
            catch (Exception ex)
            {
                // Handle exception
                return null;
            }
        }

        public async Task<bool> IsConnected(CancellationToken cancellationToken = default)
        {
            try
            {
                await Database.RunCommandAsync((Command<BsonDocument>)"{buildinfo:1}", null, cancellationToken).ConfigureAwait(false);
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}

﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using PhotoGateway.DAL.Interfaces;
using PhotoGateway.DAL.Models;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Repositories
{
    public class UserRepository : MongoRepository<UserEntity>, IUserRepository<UserEntity>
    {
        private const string CollectionName = "Users";


        public UserRepository(IOptions<MongoDbSettings> settings) : base(settings, CollectionName) { }

        public async Task<UserEntity> GetByAuth0IdAsync(string auth0Id)
        {
            var filter = Builders<UserEntity>.Filter.Eq(u => u.Auth0Id, auth0Id);
            return await this.collection.Find(filter).FirstOrDefaultAsync();
        }
    }
}

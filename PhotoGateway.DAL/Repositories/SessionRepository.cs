﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using PhotoGateway.DAL.Interfaces;
using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.Session;
using PhotoGateway.Domain.Session.Request;
using PhotoGateway.Domain.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Repositories
{
    public class SessionRepository : MongoRepository<SessionEntity>, ISessionRepository<SessionEntity>
    {
        private const string CollectionName = "Sessions";

        public SessionRepository(IOptions<MongoDbSettings> settings) : base(settings, CollectionName) { }

        public async Task<SessionWithUsersEntity> GetById(string id, string userId)
        {
            var userFilter = Builders<SessionEntity>.Filter.Or(
                Builders<SessionEntity>.Filter.Eq(u => u.PhotographerId, userId),
                Builders<SessionEntity>.Filter.Eq(u => u.ClientId, userId));

            var entityFilter = Builders<SessionEntity>.Filter.Eq(u => u.Id, id);

            var filter = Builders<SessionEntity>.Filter.And(entityFilter, userFilter);

            var userCollection = Database.GetCollection<UserEntity>("Users");
            var photographerCollection = Database.GetCollection<PhotographerEntity>("Photographers");
            var clientCollection = Database.GetCollection<ClientEntity>("Clients");

            var pipeline = await this.collection.Aggregate()
                .Match(filter)
                .Lookup(
                    foreignCollection: photographerCollection,
                    localField: s => s.PhotographerId,
                    foreignField: u => u.Id,
                    @as: (SessionWithUsersEntity s) => s.Photographer
                )
                .Unwind<SessionWithUsersEntity, SessionWithUsersEntity>(s => s.Photographer)
                .Lookup(
                    foreignCollection: userCollection,
                    localField: s => s.Photographer.UserId,
                    foreignField: u => u.Id,
                    @as: (SessionWithUsersEntity s) => s.Photographer.User
                )
                .Unwind<SessionWithUsersEntity, SessionWithUsersEntity>(s => s.Photographer.User)
                .Lookup(
                    foreignCollection: clientCollection,
                    localField: s => s.ClientId,
                    foreignField: u => u.Id,
                    @as: (SessionWithUsersEntity s) => s.Client
                )
                .Unwind<SessionWithUsersEntity, SessionWithUsersEntity>(s => s.Client)
                .Lookup(
                    foreignCollection: userCollection,
                    localField: s => s.Client.UserId,
                    foreignField: u => u.Id,
                    @as: (SessionWithUsersEntity s) => s.Client.User
                )
                .Unwind<SessionWithUsersEntity, SessionWithUsersEntity>(s => s.Client.User)
            .As<SessionWithUsersEntity>()
            .SingleOrDefaultAsync();

            return pipeline;
        }

        public async Task<List<SessionWithUsersEntity>> GetAll(string userId, ListSessionRequest request)
        {
            var userCollection = Database.GetCollection<UserEntity>("Users");
            var photographerCollection = Database.GetCollection<PhotographerEntity>("Photographers");
            var clientCollection = Database.GetCollection<ClientEntity>("Clients");

            var userFilter = Builders<SessionEntity>.Filter.Or(
                Builders<SessionEntity>.Filter.Eq(u => u.PhotographerId, userId),
                Builders<SessionEntity>.Filter.Eq(u => u.ClientId, userId));

            var sessionFilter = Builders<SessionEntity>.Filter.Empty;
            if (request.SessionStatus != null)
            {
                sessionFilter = Builders<SessionEntity>.Filter.Eq(u => u.Status, request.SessionStatus);
            }


            var filter = Builders<SessionEntity>.Filter.And(sessionFilter, userFilter);

            var pipeline = await this.collection.Aggregate()
                .Match(filter)
                .Lookup(
                    foreignCollection: photographerCollection,
                    localField: s => s.PhotographerId,
                    foreignField: u => u.Id,
                    @as: (SessionWithUsersEntity s) => s.Photographer
                )
                .Unwind<SessionWithUsersEntity, SessionWithUsersEntity>(s => s.Photographer)
                .Lookup(
                    foreignCollection: userCollection,
                    localField: s => s.Photographer.UserId,
                    foreignField: u => u.Id,
                    @as: (SessionWithUsersEntity s) => s.Photographer.User
                )
                .Unwind<SessionWithUsersEntity, SessionWithUsersEntity>(s => s.Photographer.User)
                .Lookup(
                    foreignCollection: clientCollection,
                    localField: s => s.ClientId,
                    foreignField: u => u.Id,
                    @as: (SessionWithUsersEntity s) => s.Client
                )
                .Unwind<SessionWithUsersEntity, SessionWithUsersEntity>(s => s.Client)
                .Lookup(
                    foreignCollection: userCollection,
                    localField: s => s.Client.UserId,
                    foreignField: u => u.Id,
                    @as: (SessionWithUsersEntity s) => s.Client.User
                )
                .Unwind<SessionWithUsersEntity, SessionWithUsersEntity>(s => s.Client.User)
                .As<SessionWithUsersEntity>()
                .Skip(request.Skip)
                .Limit(request.Top)
                .ToListAsync();

            return pipeline;
        }

        public async Task<IEnumerable<SessionStatistics>> GetSessionStatistics(string userId, int year)
        {
            var startOfYear = new DateTime(year, 1, 1);
            var endOfYear = new DateTime(year, 12, 31);

            var pipeline = this.collection.Aggregate()
                .Match(s => (s.PhotographerId == userId || s.ClientId == userId) && s.PhotoShootStartDate >= startOfYear && s.PhotoShootStartDate <= endOfYear)
                .Group(s => new
                {
                    Year = s.PhotoShootStartDate.Year,
                    Month = s.PhotoShootStartDate.Month
                },
                g => new
                {
                    _id = g.Key,
                    TotalCost = g.Sum(s => s.Cost ?? 0),
                    SessionCount = g.Count()
                })
                .Project(x => new SessionStatistics
                {
                    Year = x._id.Year,
                    Month = x._id.Month,
                    TotalCost = x.TotalCost,
                    SessionCount = x.SessionCount
                });

            return await pipeline.ToListAsync();
        }

        public async Task<DashboardStatistics> GetDashboardStatsAsync(string userId, UserRole role)
        {
            var stats = await this.collection.Aggregate()
                .Match(s => s.PhotographerId == userId || s.ClientId == userId)
                .Group(s => s.Status, g => new
                {
                    Status = g.Key,
                    Count = g.Count()
                })
                .ToListAsync();

            long totalUsers = 0;
            if (role == UserRole.Photographer)
            {
                totalUsers = (await this.collection
                .Aggregate()
                .Match(s => s.PhotographerId == userId)
                .Group(s => s.ClientId, g => new { ClientId = g.Key })
                .Count()
                .FirstOrDefaultAsync())?.Count ?? 0L;
            }
            else
            {
                totalUsers = (await this.collection
                .Aggregate()
                .Match(s => s.ClientId == userId)
                .Group(s => s.PhotographerId, g => new { PhotographerId = g.Key })
                .Count()
                .FirstOrDefaultAsync())?.Count ?? 0L;
            }

            var dashboardStats = new DashboardStatistics
            {
                NewProjects = stats.FirstOrDefault(x => x.Status == SessionStatus.New)?.Count ?? 0,
                ProjectsInProgress = stats.Where(x => x.Status == SessionStatus.ProjectReview || x.Status == SessionStatus.Approval || x.Status == SessionStatus.InProgress || x.Status == SessionStatus.InReview).Sum(x => x.Count),
                Completed = stats.FirstOrDefault(x => x.Status == SessionStatus.Completed)?.Count ?? 0,
                TotalUsers = totalUsers
            };

            return dashboardStats;
        }
    }
}

﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using PhotoGateway.DAL.Interfaces;
using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.PhotoReview.Request;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Repositories
{
    public class PhotoReviewRepository : MongoRepository<ReviewEntity>, IPhotoReviewRepository<ReviewEntity>
    {
        private const string CollectionName = "PhotoReviews";

        public PhotoReviewRepository(IOptions<MongoDbSettings> settings) : base(settings, CollectionName)
        {
            CreateIndexes();
        }

        public async Task<List<ReviewEntity>> GetByPhotoId(string id)
        {
            var filter = Builders<ReviewEntity>.Filter.Eq(u => u.PhotoId, id);
            return await this.collection.Find(filter).ToListAsync();
        }

        public async Task<List<ReviewExtendedEntity>> GetByPhotoId(string photoId, ListReviewsRequest request)
        {
            var userCollection = Database.GetCollection<UserEntity>("Users");

            var filter = Builders<ReviewEntity>.Filter.Eq(u => u.PhotoId, photoId);

            var result = await this.collection.Aggregate()
                .Match(filter)
                .Lookup(
                 foreignCollection: userCollection,
                 localField: s => s.UserId,
                 foreignField: u => u.Id,
                 @as: (ReviewExtendedEntity s) => s.User
                )
               .Unwind<ReviewExtendedEntity, ReviewExtendedEntity>(s => s.User)
                .As<ReviewExtendedEntity>()
                .Skip(request.Skip)
                .Limit(request.Top)
                .ToListAsync();

            return result;
        }

        private void CreateIndexes()
        {
            var photoReviewsCollection = Database.GetCollection<ReviewEntity>(CollectionName);

            var photoIdIndex = Builders<ReviewEntity>.IndexKeys.Ascending(x => x.PhotoId);

            var photoIdIndexModel = new CreateIndexModel<ReviewEntity>(photoIdIndex);

            photoReviewsCollection.Indexes.CreateOne(photoIdIndexModel);
        }
    }
}

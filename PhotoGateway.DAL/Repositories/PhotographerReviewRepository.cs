﻿using Amazon.Runtime.Internal;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using PhotoGateway.DAL.Interfaces;
using PhotoGateway.DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Repositories
{
    public class PhotographerReviewRepository : MongoRepository<PhotographerReviewEntity>, IPhotographerReviewRepository<PhotographerReviewEntity>
    {
        private const string CollectionName = "PhotographerReviews";

        public PhotographerReviewRepository(IOptions<MongoDbSettings> settings) : base(settings, CollectionName)
        {
            CreateIndexes();
        }


        public async Task<List<PhotographerReviewEntityExtended>> GetByPhotographerId(string photographerId)
        {
            var userCollection = Database.GetCollection<UserEntity>("Users");


            var filter = Builders<PhotographerReviewEntity>.Filter.Eq(u => u.PhotographerId, photographerId);

            var pipeline = await this.collection.Aggregate()
                .Match(filter)
                .Lookup(
                    foreignCollection: userCollection,
                    localField: s => s.ClientId,
                    foreignField: u => u.Id,
                    @as: (PhotographerReviewEntityExtended s) => s.User
                )
                .Unwind<PhotographerReviewEntityExtended, PhotographerReviewEntityExtended>(s => s.User)
            .As<PhotographerReviewEntityExtended>()
            .ToListAsync();

            return pipeline;
        }

        private void CreateIndexes()
        {
            var photographerReviewsCollection = Database.GetCollection<PhotographerReviewEntity>(CollectionName);

            var photographerIdIndex = Builders<PhotographerReviewEntity>.IndexKeys.Ascending(x => x.PhotographerId);
            var clientIdIndex = Builders<PhotographerReviewEntity>.IndexKeys.Ascending(x => x.ClientId);

            var photographerIdIndexModel = new CreateIndexModel<PhotographerReviewEntity>(photographerIdIndex);
            var clientIdIndexModel = new CreateIndexModel<PhotographerReviewEntity>(clientIdIndex);

            photographerReviewsCollection.Indexes.CreateMany(new[]
            {
                photographerIdIndexModel,
                clientIdIndexModel
            });
        }
    }
}

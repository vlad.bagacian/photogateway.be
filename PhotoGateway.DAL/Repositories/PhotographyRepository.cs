﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using PhotoGateway.DAL.Interfaces;
using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.Photographer.Response;
using PhotoGateway.Domain.Photography.Request;
using PhotoGateway.Domain.Session.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Repositories
{
    public class PhotographyRepository : MongoRepository<PhotographyEntity>, IPhotographyRepository<PhotographyEntity>
    {
        private const string CollectionName = "Photographies";

        public PhotographyRepository(IOptions<MongoDbSettings> settings) : base(settings, CollectionName)
        {
            CreateIndexes();
        }

        public async Task AddManyAsync(List<PhotographyEntity> photograpies)
        {
            await this.collection.InsertManyAsync(photograpies);
        }

        public async Task<List<PhotographyEntity>> GetAll(string sessionId)
        {
            var filter = Builders<PhotographyEntity>.Filter.Eq(u => u.SessionId, sessionId);
            if (string.IsNullOrEmpty(sessionId))
            {
                filter = Builders<PhotographyEntity>.Filter.Exists(u => u.Id);
            }

            return await this.collection.Find(filter).ToListAsync();
        }

        public async Task<List<PhotographyEntity>> GetAllAsync(string sessionId, ListPhotosRequest request = null)
        {
            var sessionFilter = Builders<PhotographyEntity>.Filter.Eq(u => u.SessionId, sessionId);

            var additionalFilter = Builders<PhotographyEntity>.Filter.Empty;

            var filter = Builders<PhotographyEntity>.Filter.And(sessionFilter, additionalFilter);

            if (request != null)
            {
                return await this.collection.Find(filter).Skip(request.Skip).Limit(request.Top).ToListAsync();
            }
            else
            {
                return await this.collection.Find(filter).ToListAsync();
            }
        }

        public async Task<List<PhotographyEntity>> GetAllByIds(HashSet<string> ids)
        {
            try
            {
                var filter = Builders<PhotographyEntity>.Filter.In(e => e.Id, ids);
                var result = await collection.Find(filter).ToListAsync();

                return result;
            }
            catch (Exception ex)
            {
                // Handle exception
                return null;
            }
        }

        private void CreateIndexes()
        {
            var photographiesCollection = Database.GetCollection<PhotographyEntity>(CollectionName);

            var sessionIdIndex = Builders<PhotographyEntity>.IndexKeys.Ascending(x => x.SessionId);

            var sessionIdIndexModel = new CreateIndexModel<PhotographyEntity>(sessionIdIndex);

            photographiesCollection.Indexes.CreateOne(sessionIdIndexModel);
        }
    }
}
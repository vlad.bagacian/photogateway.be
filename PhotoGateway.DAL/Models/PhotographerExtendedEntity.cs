﻿namespace PhotoGateway.DAL.Models
{
    public class PhotographerExtendedEntity : PhotographerEntity
    {
        public UserEntity User { get; set; }
    }
}

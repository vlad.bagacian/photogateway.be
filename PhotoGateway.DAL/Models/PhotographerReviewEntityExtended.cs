﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoGateway.DAL.Models
{
    public class PhotographerReviewEntityExtended : PhotographerReviewEntity
    {
        public UserEntity User { get; set; }
    }
}

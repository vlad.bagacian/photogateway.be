﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace PhotoGateway.DAL.Models
{
    public class ReviewEntity : BaseEntity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string PhotoId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }

        public string Comment { get; set; }
    }
}

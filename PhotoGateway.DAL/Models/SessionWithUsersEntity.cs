﻿namespace PhotoGateway.DAL.Models
{
    public class SessionWithUsersEntity : SessionEntity
    {
        public PhotographerExtendedEntity Photographer { get; set; }

        public ClientExtendedEntity Client { get; set; }
    }
}

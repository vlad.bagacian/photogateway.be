﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System;

namespace PhotoGateway.DAL.Models
{
    public class PhotographerReviewEntity : BaseEntity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string PhotographerId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string ClientId { get; set; }

        public decimal Rating { get; set; }

        public string Comment { get; set; }
    }
}

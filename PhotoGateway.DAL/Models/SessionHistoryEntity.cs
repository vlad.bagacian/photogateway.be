﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using PhotoGateway.Domain.Session;
using System;

namespace PhotoGateway.DAL.Models
{
    public class SessionHistoryEntity
    {
        public DateTime DateTime { get; set; }

        [BsonRepresentation(BsonType.String)]
        public SessionStatus Status { get; set; }
    }
}

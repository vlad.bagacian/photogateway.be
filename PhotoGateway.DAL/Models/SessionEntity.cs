﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using PhotoGateway.Domain.Session;
using System;
using System.Collections.Generic;

namespace PhotoGateway.DAL.Models
{
    public class SessionEntity : BaseEntity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string PhotographerId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string ClientId { get; set; }

        public DateTime? StartedAt { get; set; }

        public DateTime DueAt { get; set; }

        public DateTime PhotoShootStartDate { get; set; }

        public DateTime PhotoShootEndDate { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int PhotoCount { get; set; }

        public string Location { get; set; }

        [BsonRepresentation(BsonType.Decimal128)]
        public decimal? Cost { get; set; }

        [BsonRepresentation(BsonType.String)]
        public SessionStatus Status { get; set; }

        public List<SessionHistoryEntity> SessionHistory { get; set; }

        public void MoveNext()
        {
            switch (this.Status)
            {
                case SessionStatus.New:
                    Status = SessionStatus.ProjectReview;
                    break;
                case SessionStatus.ProjectReview:
                    Status = SessionStatus.Approval;
                    break;
                case SessionStatus.Approval:
                    Status = SessionStatus.InProgress;
                    this.StartedAt = DateTime.UtcNow;
                    break;
                case SessionStatus.InProgress:
                    Status = SessionStatus.InReview;
                    break;
                case SessionStatus.InReview:
                    Status = SessionStatus.Completed;
                    break;
                default:
                    throw new InvalidOperationException("Invalid state transition");
            }

            this.ModifiedAt = DateTime.UtcNow;

            this.SessionHistory.Add(new SessionHistoryEntity
            {
                DateTime = DateTime.UtcNow,
                Status = this.Status
            });

        }

        public void MoveBack()
        {
            switch (this.Status)
            {
                case SessionStatus.New:
                case SessionStatus.Approval:
                case SessionStatus.ProjectReview:
                    Status = SessionStatus.Cancelled;
                    break;
                default:
                    throw new InvalidOperationException("Invalid state transition");
            }

            this.ModifiedAt = DateTime.UtcNow;

            this.SessionHistory.Add(new SessionHistoryEntity
            {
                DateTime = DateTime.UtcNow,
                Status = this.Status
            });
        }
    }
}

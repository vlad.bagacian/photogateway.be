﻿using PhotoGateway.Domain.Photographer;
using System;

namespace PhotoGateway.DAL.Models.Filters
{
    public class PhotographerFilter
    {
        public string Location { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public PhotographySkill Skill { get; set; }
    }
}

﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using PhotoGateway.Domain.Photographer;
using System;

namespace PhotoGateway.DAL.Models
{
    public class PhotographyEntity : BaseEntity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string SessionId { get; set; }

        public PhotoMetadataEntity Metadata { get; set; }

        public string CameraMake { get; set; }

        public string CameraModel { get; set; }

        public ExposureSettings ExposureSettings { get; set; }

        public bool Selected { get; set; }
    }
}

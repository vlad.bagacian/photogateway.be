﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using PhotoGateway.Domain.Photographer;

namespace PhotoGateway.DAL.Models
{
    public class PhotographerEntity : UserBaseEntity
    {
        public string Biography { get; set; }

        public List<PhotographySkill> Skills { get; set; }

        public List<PhotoMetadataEntity> PortfolioPhotos { get; set; }

        public string Location { get; set; }

        public bool IsProfileCompleted { get; set; }

        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Cost { get; set; }

        public decimal Rating { get; set; }

        public string FacebookProfile { get; set; }

        public string InstagramProfile { get; set; }
    }
}

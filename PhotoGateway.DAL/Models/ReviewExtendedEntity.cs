﻿namespace PhotoGateway.DAL.Models
{
    public class ReviewExtendedEntity : ReviewEntity
    {
        public UserEntity User { get; set; }
    }
}

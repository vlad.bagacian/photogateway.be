﻿namespace PhotoGateway.DAL.Models
{
    public class ClientExtendedEntity : ClientEntity
    {
        public UserEntity User { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace PhotoGateway.DAL.Models
{
    public class PhotoMetadataEntity
    {
        public string ImageUrl { get; set; }

        public decimal Ratio { get; set; }

        public List<string> History { get; set; }
    }
}

﻿using PhotoGateway.Domain.UserModel;

namespace PhotoGateway.DAL.Models
{
    public class UserEntity : BaseEntity
    {
        public string Auth0Id { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public bool EmailVerified { get; set; }

        public Address Address { get; set; }

        public string ImageUrl { get; set; }

        public UserRole Role { get; set; }

        public string GetFullName()
        {
            return $"{this.FirstName} {this.LastName}";
        }
    }
}

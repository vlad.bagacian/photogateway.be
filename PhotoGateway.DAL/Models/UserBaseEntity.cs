﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace PhotoGateway.DAL.Models
{
    public abstract class UserBaseEntity : BaseEntity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }
    }
}

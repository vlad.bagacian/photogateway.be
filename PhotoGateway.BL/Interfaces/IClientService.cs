﻿using PhotoGateway.DAL.Models;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Interfaces
{
    public interface IClientService : IService
    {
        Task AddAsync(ClientEntity client);
        Task<ClientEntity> GetByIdAsync(string id);
        Task UpdateAsync(ClientEntity photographer);
    }
}

﻿using Microsoft.AspNetCore.Http;
using PhotoGateway.DAL.Models;
using PhotoGateway.Domain;
using PhotoGateway.Domain.App;
using SixLabors.ImageSharp;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Interfaces
{
    public interface IFileManagementService : IService
    {
        Task<string> UploadFileAsync(string userId, Stream file);
        Task<List<PhotographyEntity>> UploadPhotosAsync(string userId, List<IFormFile> photos);
    }
}

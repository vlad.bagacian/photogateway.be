﻿using PhotoGateway.DAL.Models;
using PhotoGateway.Domain;
using PhotoGateway.Domain.App;
using PhotoGateway.Domain.Photographer;
using PhotoGateway.Domain.Photographer.Request;
using PhotoGateway.Domain.Photographer.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Interfaces
{
    public interface IPhotographerService : IService
    {
        Task AddAsync(PhotographerEntity photographer);
        Task<Photographer> GetByIdAsync(string id);
        Task<PhotographerProfile> GetAccount(RequestContext requestContext);
        Task<PhotographerProfile> UpdateProfile(RequestContext requestContext, PhotographerUpdateRequest updateRequest);
        Task<List<PhotographerResponse>> GetPhotographersAsync(ListPhotographersRequest request);
        Task<Photographer> UpdatePortfolio(RequestContext requestContext, List<PhotoMetadataEntity> portfolioPhotos);
        Task<PhotographerProfile> GetProfile(string id);
        Task<List<PhotographerReviewResponse>> GetReviews(string photographerId);
        Task<List<PhotographerReviewResponse>> AddReview(RequestContext requestContext, string photographerId, PhotographerReviewRequest request);
    }
}
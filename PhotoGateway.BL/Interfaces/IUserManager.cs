﻿using PhotoGateway.Domain.UserModel.Request;
using PhotoGateway.Domain.UserModel.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Interfaces
{
    public interface IUserManager : IService
    {
        Task CreateUserAsync(UserRequest userRequest);

        Task<UserResponse> GetUserByAuth0Id(string auth0Id);
        Task<List<UserResponse>> GetUsersByIds(HashSet<string> userIds);
        Task<UserResponse> UpdateUserAsync(string userId, UpdateUserRequest updateUserRequest);
        Task<UserResponse> UpdateUserAvatarAsync(string userId, string avatarUrl);
    }
}
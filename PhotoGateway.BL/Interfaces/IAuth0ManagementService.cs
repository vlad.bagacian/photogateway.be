﻿using PhotoGateway.Domain.UserModel.Request;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Interfaces
{
    public interface IAuth0ManagementService : IService
    {
        Task<(string userId, string pictureUrl)> CreateUserAsync(UserRequest userRequest, CancellationToken cancellationToken = default);
    }
}
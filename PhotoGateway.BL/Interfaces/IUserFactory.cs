﻿using PhotoGateway.DAL.Models;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Interfaces
{
    public interface IUserFactory : IService
    {
        Task<UserBaseEntity> CreateUser(UserEntity appUser);
        Task<string> GetId(UserEntity userEntity);
    }
}

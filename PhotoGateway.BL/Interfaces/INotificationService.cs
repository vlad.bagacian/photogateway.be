﻿using PhotoGateway.DAL.Models;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Interfaces
{
    public interface INotificationService : IService
    {
        Task NotifySessionUpdates(SessionWithUsersEntity session);
    }
}

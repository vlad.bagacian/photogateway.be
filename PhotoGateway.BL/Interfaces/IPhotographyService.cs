﻿using Microsoft.AspNetCore.Http;
using PhotoGateway.DAL.Models;
using PhotoGateway.Domain;
using PhotoGateway.Domain.App;
using PhotoGateway.Domain.Photography.Request;
using PhotoGateway.Domain.PhotoReview.Request;
using PhotoGateway.Domain.PhotoReview.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Interfaces
{
    public interface IPhotographyService : IService
    {
        Task AddAsync(PhotographyEntity photography);
        Task AddManyAsync(List<PhotographyEntity> result);
        Task<List<PhotoReviewResponse>> AddReviewAsync(RequestContext requestContext, string photoId,AddCommentRequest request);
        Task<List<PhotographyResponse>> GetAllPhotosAsync(string sessionId, ListPhotosRequest request);

        Task<List<PhotographyResponse>> GetAllPhotosInSession(string sessionId);

        Task<PhotographyResponse> GetPhotoAsync(string photoId);
        Task<List<PhotoReviewResponse>> GetReviewsAsync(RequestContext requestContext, string photoId, ListReviewsRequest request);
        Task<PhotographyResponse> MarkAsSelected(string photoId);
        Task<PhotographyResponse> UpdatePhotoAsync(string photoId, PhotographyEntity photographyEntity);
    }
}

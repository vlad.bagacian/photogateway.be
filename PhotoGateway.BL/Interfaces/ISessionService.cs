﻿using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.App;
using PhotoGateway.Domain.Session;
using PhotoGateway.Domain.Session.Request;
using PhotoGateway.Domain.Session.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Interfaces
{
    public interface ISessionService : IService
    {
        Task<SessionResponse> CreateSessionAsync(string userId, CreateSessionRequest sessionRequest);

        Task<List<SessionResponse>> GetAllSessionsAsync(string userId, ListSessionRequest request);

        Task<SessionResponse> GetSessionAsync(string userId, string sessionId);
        Task<DashboardStatistics> GetSessionStatistics(RequestContext requestContext, int year);
        Task<SessionResponse> MoveBack(string userId, string sessionId);

        Task<SessionResponse> MoveNext(string userId, string sessionId);

        Task<SessionResponse> UpdateSessionAsync(string userId, string sessionId, UpdateSessionRequest request);
    }
}

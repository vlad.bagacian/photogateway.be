﻿using MongoDB.Bson;
using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.Session;
using PhotoGateway.Domain.Session.Request;
using PhotoGateway.Domain.Session.Response;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PhotoGateway.BL.Extensions
{
    public static class SessionExtensions
    {
        public static SessionEntity ToEntity(this CreateSessionRequest request, string clientId) => new()
        {
            Id = ObjectId.GenerateNewId().ToString(),
            Title = request.Title,
            Description = request.Description,
            PhotographerId = request.PhotographerId,
            ClientId = clientId,
            CreatedAt = DateTime.UtcNow,
            DueAt = request.DueAt,
            PhotoShootStartDate = request.PhotoShootStartDate,
            PhotoShootEndDate = request.PhotoShootEndDate,
            PhotoCount = request.PhotoCount,
            Location = request.Location,
            Status = SessionStatus.New,
            SessionHistory = new List<SessionHistoryEntity>
            {
                new SessionHistoryEntity
                {
                    Status = SessionStatus.New,
                    DateTime = DateTime.UtcNow
                }
            }
        };

        public static SessionResponse ToResponse(this SessionWithUsersEntity entity) => new()
        {
            Id = entity.Id,
            StartedAt = entity.StartedAt,
            CreatedAt = entity.CreatedAt,
            PhotographerId = entity.PhotographerId,
            PhotographerName = $"{entity.Photographer.User.FirstName} {entity.Photographer.User.LastName}",
            PhotographerAvatar = entity.Photographer.User.ImageUrl,
            PhotographerEmail = entity.Photographer.User.Email,
            ClientId = entity.ClientId,
            ClientAvatar = entity.Client.User.ImageUrl,
            ClientName = $"{entity.Client.User.FirstName} {entity.Client.User.LastName}",
            ClientEmail = entity.Client.User.Email,
            PhotoShootStartDate = entity.PhotoShootStartDate,
            PhotoShootEndDate = entity.PhotoShootEndDate,
            DueAt = entity.DueAt,
            Title = entity.Title,
            Description = entity.Description,
            Location = entity.Location,
            Cost = entity.Cost,
            Status = entity.Status,
            SessionHistory = entity.SessionHistory.Select(s=>s.ToResponse()).ToList()
        };

        public static SessionResponse ToResponse(this SessionEntity entity) => new()
        {
            Id = entity.Id,
            StartedAt = entity.StartedAt,
            PhotographerId = entity.PhotographerId,
            ClientId = entity.ClientId,
            PhotoShootStartDate = entity.PhotoShootStartDate,
            PhotoShootEndDate = entity.PhotoShootEndDate,
            DueAt = entity.DueAt,
            Description = entity.Description,
            Title = entity.Title,
            Location = entity.Location,
            Cost = entity.Cost,
            Status = entity.Status,
            SessionHistory = entity.SessionHistory.Select(s=>s.ToResponse()).ToList()
        };

        public static SessionHistory ToResponse(this SessionHistoryEntity entity) => new()
        {
            DateTime = entity.DateTime,
            Status = entity.Status
        };

        public static SessionEntity ToEntity(this SessionWithUsersEntity extendedSession) => new()
        {
            Id = extendedSession.Id,
            PhotographerId = extendedSession.PhotographerId,
            ClientId = extendedSession.ClientId,
            CreatedAt = extendedSession.CreatedAt,
            DueAt = extendedSession.DueAt,
            Cost = extendedSession.Cost,
            Title = extendedSession.Title,
            Description = extendedSession.Description,
            StartedAt = extendedSession.StartedAt,
            PhotoShootStartDate = extendedSession.PhotoShootStartDate,
            PhotoShootEndDate = extendedSession.PhotoShootEndDate,
            PhotoCount = extendedSession.PhotoCount,
            Location = extendedSession.Location,
            Status = extendedSession.Status,
            SessionHistory = extendedSession.SessionHistory
        };

        public static void Update(this SessionEntity entity, UpdateSessionRequest update)
        {
            if (!string.IsNullOrEmpty(update.Description))
            {
                entity.Description = update.Description;
            }

            if (!string.IsNullOrEmpty(update.Title))
            {
                entity.Title = update.Title;
            }

            if (update.Cost is not null)
            {
                entity.Cost = update.Cost;
            }

            if (update.DueAt is not null)
            {
                entity.DueAt = (DateTime)update.DueAt;
            }
        }

        public static void SetStatus(this SessionEntity entity, SessionStatus newStatus)
        {
            entity.Status = newStatus;
        }
    }
}

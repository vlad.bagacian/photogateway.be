﻿using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.App;
using PhotoGateway.Domain.PhotoReview.Response;

namespace PhotoGateway.BL.Extensions
{
    public static class PhotoReviewExtensions
    {
        public static PhotoReviewResponse ToResponse(this ReviewExtendedEntity entity, RequestContext requestContext) => new()
        {
            Id = entity.Id,
            Comment = entity.Comment,
            CreatedAt = entity.CreatedAt,
            UserName = $"{entity.User.FirstName} {entity.User.LastName}",
            ImageUrl = entity.User.ImageUrl,
            Sent = entity.User.Id == requestContext.AccountId
        };
    }
}

﻿using PhotoGateway.DAL.Models;
using PhotoGateway.Domain;

namespace PhotoGateway.BL.Extensions
{
    public static class PhotographyExtensions
    {
        public static PhotographyResponse ToPhotography(this PhotographyEntity entity) => new PhotographyResponse
        {
            Id = entity.Id,
            CreatedAt = entity.CreatedAt,
            Metadata = entity.Metadata.ToResponse(),
            ExposureSettings = entity.ExposureSettings,
            CameraMake = entity.CameraMake,
            CameraModel = entity.CameraModel
        };

        public static PhotoMetadata ToResponse(this PhotoMetadataEntity entity) => new()
        {
            ImageUrl = entity.ImageUrl,
            Ratio = entity.Ratio,
            History = entity.History
        };

        public static PhotoMetadataEntity ToEntity(this PhotoMetadata metadata) => new()
        {
            ImageUrl = metadata.ImageUrl,
            Ratio = metadata.Ratio
        };

        public static PhotographyEntity ToEntity2(this PhotographyResponse photographer) => new PhotographyEntity
        {
            Id = photographer.Id,
            CreatedAt = photographer.CreatedAt,
            Metadata = photographer.Metadata.ToEntity(),
            ExposureSettings = photographer.ExposureSettings,
            CameraMake = photographer.CameraMake,
            CameraModel = photographer.CameraModel
        };

        public static PhotographyResponse ToResponse(this PhotographyEntity entity) => new()
        {
            Id = entity.Id,
            CreatedAt = entity.CreatedAt,
            Metadata = entity.Metadata.ToResponse(),
            CameraMake = entity.CameraMake,
            CameraModel = entity.CameraModel,
            ExposureSettings = entity.ExposureSettings,
            Selected = entity.Selected
        };
    }
}

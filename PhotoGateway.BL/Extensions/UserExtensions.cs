﻿using MongoDB.Bson;
using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.Photographer;
using PhotoGateway.Domain.UserModel.Request;
using PhotoGateway.Domain.UserModel.Response;
using System.Collections.Generic;

namespace PhotoGateway.BL.Extensions
{
    public static class UserExtensions
    {
        public static UserResponse ToUser(this UserEntity userEntity, string eid)
        {
            if (userEntity == null)
            {
                return null;
            }

            return new UserResponse
            {
                Id = userEntity.Id,
                Auth0Id = userEntity.Auth0Id,
                Email = userEntity.Email,
                FirstName = userEntity.FirstName,
                LastName = userEntity.LastName,
                Phone = userEntity.Phone,
                EmailVerified = userEntity.EmailVerified,
                Address = userEntity.Address,
                ProfileImageUrl = userEntity.ImageUrl,
                Role = userEntity.Role,
                EId = eid
            };
        }

        public static UserResponse ToResponse(this UserEntity userEntity, string eid = null)
        {
            if (userEntity == null)
            {
                return null;
            }

            return new UserResponse
            {
                Id = userEntity.Id,
                Auth0Id = userEntity.Auth0Id,
                Email = userEntity.Email,
                FirstName = userEntity.FirstName,
                LastName = userEntity.LastName,
                Phone = userEntity.Phone,
                EmailVerified = userEntity.EmailVerified,
                Address = userEntity.Address,
                ProfileImageUrl = userEntity.ImageUrl,
                Role = userEntity.Role,
                EId = eid
            };
        }

        public static UserProfile ToProfile(this UserResponse user) => new()
        {
            UserId = user.Id,
            Email = user.Email,
            FirstName = user.FirstName,
            LastName = user.LastName,
            Phone = user.Phone,
            Address = user.Address,
            ProfileImageUrl = user.ProfileImageUrl,
            Role = user.Role.ToString()
        };

        public static UserEntity ToUser(this UserRequest userRequest, string auth0Id, string profileImage)
        {
            return new UserEntity
            {
                Id = ObjectId.GenerateNewId().ToString(),
                Auth0Id = auth0Id,
                Email = userRequest.Email,
                FirstName = userRequest.FirstName,
                LastName = userRequest.LastName,
                Phone = userRequest.Phone,
                Address = userRequest.Address,
                ImageUrl = profileImage,
                Role = userRequest.Role
            };
        }

        public static PhotographerEntity ToPhotographer(this UserEntity userEntity) => new PhotographerEntity
        {
            Id = ObjectId.GenerateNewId().ToString(),
            UserId = userEntity.Id,
            Location = userEntity.Address.City
        };

        public static ClientEntity ToClient(this UserEntity userEntity) => new ClientEntity
        {
            Id = ObjectId.GenerateNewId().ToString(),
            UserId = userEntity.Id
        };
    }
}

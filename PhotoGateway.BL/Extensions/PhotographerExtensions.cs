﻿using PhotoGateway.DAL.Models;
using PhotoGateway.Domain;
using PhotoGateway.Domain.Photographer;
using PhotoGateway.Domain.Photographer.Response;
using System.Collections.Generic;
using System.Linq;

namespace PhotoGateway.BL.Extensions
{
    public static class PhotographerExtensions
    {
        public static Photographer ToPhotographer(this PhotographerEntity photographerEntity)
        {
            return new()
            {
                Id = photographerEntity.Id,
                UserId = photographerEntity.UserId,
                Biography = photographerEntity.Biography,
                Skills = photographerEntity.Skills,
                IsProfileCompleted = photographerEntity.IsProfileCompleted,
                Location = photographerEntity.Location
            };
        }

        public static PhotographerProfile ToProfile(this PhotographerExtendedEntity entity)
        {
            return new()
            {
                Id = entity.Id,
                UserId = entity.UserId,
                Biography = entity.Biography,
                IsProfileCompleted = entity.IsProfileCompleted,
                Skills = entity.Skills,
                FirstName = entity.User.FirstName,
                Email = entity.User.Email,
                LastName = entity.User.LastName,
                Phone = entity.User.Phone,
                Address = entity.User.Address,
                ProfileImageUrl = entity.User.ImageUrl,
                Role = entity.User.Role.ToString(),
                Rating = entity.Rating,
                Cost = entity.Cost,
                PortfolioPhotos = entity.PortfolioPhotos?.Select(d=>d.ToResponse())?.ToList() ?? new List<PhotoMetadata>(),
                FacebookProfile = entity.FacebookProfile,
                InstagramProfile = entity.InstagramProfile
            };
        }

        public static PhotographerEntity ToEntity(this Photographer photographer) => new PhotographerEntity
        {
            Id = photographer.Id,
            UserId = photographer.UserId,
            Biography = photographer.Biography,
            Skills = photographer.Skills,
            //PortfolioId = photographer.Portfolio.Id,
            IsProfileCompleted = photographer.IsProfileCompleted,
            Location = photographer.Location,
            FacebookProfile = photographer.FacebookProfile,
            InstagramProfile = photographer.InstagramProfile
        };

        public static PhotographerEntity ToEntity(this PhotographerExtendedEntity extendedEntity) => new()
        {
            Id = extendedEntity.Id,
            CreatedAt = extendedEntity.CreatedAt,
            ModifiedAt = extendedEntity.ModifiedAt,
            UserId = extendedEntity.UserId,
            Biography = extendedEntity.Biography,
            Skills = extendedEntity.Skills,
            PortfolioPhotos = extendedEntity.PortfolioPhotos,
            Location = extendedEntity.Location,
            IsProfileCompleted = extendedEntity.IsProfileCompleted,
            Cost = extendedEntity.Cost,
            Rating = extendedEntity.Rating,
            FacebookProfile = extendedEntity.FacebookProfile,
            InstagramProfile = extendedEntity.InstagramProfile
        };

        public static PhotographerResponse ToResponse(this PhotographerExtendedEntity entity) => new()
        {
            Id = entity.Id,
            FirstName = entity.User.FirstName,
            LastName = entity.User.LastName,
            Biography = entity.Biography,
            Rating = entity.Rating,
            PortfolioPhotos = entity.PortfolioPhotos.Select(d => d.ToResponse()).ToList(),
            Cost = entity.Cost,
            ProfileImageUrl = entity.User.ImageUrl,
            Skills = entity.Skills,
            FacebookProfile = entity.FacebookProfile,
            InstagramProfile = entity.InstagramProfile
        };


        public static PhotographerReviewEntity ToEntity(this PhotographerReviewRequest request, string photographerId, string clientId) => new()
        {
            ClientId = clientId,
            PhotographerId = photographerId,
            Comment = request.Comment,
            Rating = request.Rating
        };

        public static PhotographerReviewResponse ToResponse(this PhotographerReviewEntityExtended entity) => new()
        {
            ClientName = $"{entity.User.FirstName} {entity.User.LastName}",
            Comment = entity.Comment,
            Rating = entity.Rating,
            CreatedAt = entity.CreatedAt
        };
    }
}

﻿using Auth0.AuthenticationApi;
using Auth0.AuthenticationApi.Models;
using Auth0.ManagementApi;
using Auth0.ManagementApi.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.Domain.App;
using PhotoGateway.Domain.UserModel.Request;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Services
{
    public class Auth0ManagementService : IAuth0ManagementService
    {
        private readonly static string UserPasswordConnection = "Username-Password-Authentication";
        private readonly IMemoryCache memoryCache;
        private readonly Auth0Settings auth0Settings;
        private readonly ILogger logger;

        private readonly Lazy<IAuthenticationApiClient> authenticationApiClient;

        public Auth0ManagementService(IOptions<Auth0Settings> options, IMemoryCache memoryCache, ILogger<Auth0ManagementService> logger)
        {
            this.auth0Settings = options.Value;
            this.memoryCache = memoryCache;
            this.logger = logger;
            this.authenticationApiClient = new Lazy<IAuthenticationApiClient>(() => new AuthenticationApiClient(this.auth0Settings.ClientDomain));
        }

        public async Task<(string userId, string pictureUrl)> CreateUserAsync(UserRequest userRequest, CancellationToken cancellationToken = default)
        {
            string accessToken = await this.GetAuth0ManagementAccessToken(cancellationToken).ConfigureAwait(false);

            ManagementApiClient managementApiClient = new ManagementApiClient(accessToken, this.auth0Settings.ClientDomain);

            UserCreateRequest userCreateRequest = ToAuth0Request(userRequest);

            userCreateRequest.Connection = Auth0ManagementService.UserPasswordConnection;
            User result = await managementApiClient.Users.CreateAsync(userCreateRequest, cancellationToken).ConfigureAwait(false);

            Role role = (await this.GetRoles()).FirstOrDefault(r => r.Name.Equals(userRequest.Role.ToString()));

            await this.AddUserToRole(result.UserId, role.Id, cancellationToken);

            return (result.UserId, result.Picture);
        }

        private async Task AddUserToRole(string userId, string roleId, CancellationToken cancellationToken)
        {
            string accessToken = await this.GetAuth0ManagementAccessToken(cancellationToken).ConfigureAwait(false);

            ManagementApiClient managementApiClient = new ManagementApiClient(accessToken, this.auth0Settings.ClientDomain);

            await managementApiClient.Users.AssignRolesAsync(userId, new AssignRolesRequest() { Roles = new[] { roleId } }, cancellationToken).ConfigureAwait(false);
        }

        private async Task<string> GetAuth0ManagementAccessToken(CancellationToken cancellationToken = default)
        {
            string audience = new Uri($"https://{this.auth0Settings.ClientDomain}/api/v2/").ToString();

            memoryCache.TryGetValue(audience, out string accessToken);
            if (!string.IsNullOrEmpty(accessToken))
            {
                logger.LogInformation("Get access token from cache");
                return accessToken;
            }

            var request = new ClientCredentialsTokenRequest
            {
                ClientId = this.auth0Settings.ClientId,
                ClientSecret = this.auth0Settings.ClientSecret,
                Audience = audience,
            };

            AccessTokenResponse token = await this.authenticationApiClient.Value.GetTokenAsync(request, cancellationToken).ConfigureAwait(false);

            var cacheEntryOptions = new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Now.AddMilliseconds(token.ExpiresIn)
            };

            memoryCache.Set(audience, token.AccessToken, cacheEntryOptions);

            return token.AccessToken;
        }

        private async Task<Role[]> GetRoles(CancellationToken cancellationToken = default)
        {
            string accessToken = await this.GetAuth0ManagementAccessToken(cancellationToken).ConfigureAwait(false);

            memoryCache.TryGetValue(nameof(GetRoles), out Role[] roles);
            if (roles != null && roles.Length != 0)
            {
                logger.LogInformation("Get roles from cache");
                return roles;
            }

            ManagementApiClient managementApiClient = new ManagementApiClient(accessToken, this.auth0Settings.ClientDomain);
            var result = (await managementApiClient.Roles.GetAllAsync(new GetRolesRequest(), cancellationToken).ConfigureAwait(false))?.ToArray();

            var cacheEntryOptions = new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Now.AddHours(1)
            };
            memoryCache.Set(nameof(GetRoles), result, cacheEntryOptions);

            return result;
        }

        private UserCreateRequest ToAuth0Request(UserRequest userRequest)
        {
            var userCreateRequest = new UserCreateRequest
            {
                Connection = "Username-Password-Authentication",
                Email = userRequest.Email,
                Password = userRequest.Password,
                FirstName = userRequest.FirstName,
                LastName = userRequest.LastName,
                AppMetadata = new
                {
                    role = userRequest.Role.ToString()
                }
            };

            return userCreateRequest;
        }
    }
}
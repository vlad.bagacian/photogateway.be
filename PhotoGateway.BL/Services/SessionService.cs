﻿using PhotoGateway.BL.Extensions;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.DAL.Interfaces;
using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.App;
using PhotoGateway.Domain.Exceptions;
using PhotoGateway.Domain.Session;
using PhotoGateway.Domain.Session.Request;
using PhotoGateway.Domain.Session.Response;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Services
{
    public class SessionService : ISessionService
    {
        private ISessionRepository<SessionEntity> sessionRepository;
        private INotificationService notificationService;

        public SessionService(ISessionRepository<SessionEntity> sessionRepository, INotificationService notificationService)
        {
            this.sessionRepository = sessionRepository;
            this.notificationService = notificationService;
        }

        public async Task<SessionResponse> CreateSessionAsync(string userId, CreateSessionRequest sessionRequest)
        {
            SessionEntity entity = sessionRequest.ToEntity(userId);
            await this.sessionRepository.AddAsync(entity);

            var updatedSession = await this.sessionRepository.GetById(entity.Id, userId).ConfigureAwait(false);
            await this.notificationService.NotifySessionUpdates(updatedSession).ConfigureAwait(false);

            return entity.ToResponse();
        }

        public async Task<SessionResponse> GetSessionAsync(string userId, string sessionId)
        {
            var entity = await this.sessionRepository.GetById(sessionId, userId).ConfigureAwait(false);
            if (entity == null)
            {
                throw new NotFoundException("The session was not found", "NOT_FOUND");
            }

            return entity.ToResponse();
        }

        public async Task<SessionResponse> UpdateSessionAsync(string userId, string sessionId, UpdateSessionRequest request)
        {
            var entity = await this.sessionRepository.GetById(sessionId, userId).ConfigureAwait(false);
            if (entity == null)
            {
                throw new NotFoundException("The session was not found", "NOT_FOUND");
            }

            entity.Update(request);

            await this.sessionRepository.UpdateAsync(entity.ToEntity()).ConfigureAwait(false);

            return entity.ToResponse();
        }

        public async Task<SessionResponse> MoveNext(string userId, string sessionId)
        {
            var entity = await this.sessionRepository.GetById(sessionId, userId).ConfigureAwait(false);
            if (entity == null)
            {
                throw new NotFoundException("The session was not found", "NOT_FOUND");
            }
            entity.MoveNext();
            await this.notificationService.NotifySessionUpdates(entity).ConfigureAwait(false);
            await this.sessionRepository.UpdateAsync(entity.ToEntity()).ConfigureAwait(false);
            return entity.ToResponse();
        }

        public async Task<SessionResponse> MoveBack(string userId, string sessionId)
        {
            var entity = await this.sessionRepository.GetById(sessionId, userId).ConfigureAwait(false);
            if (entity == null)
            {
                throw new NotFoundException("The session was not found", "NOT_FOUND");
            }
            entity.MoveBack();
            await this.notificationService.NotifySessionUpdates(entity).ConfigureAwait(false);
            await this.sessionRepository.UpdateAsync(entity.ToEntity()).ConfigureAwait(false);
            return entity.ToResponse();
        }

        public async Task<List<SessionResponse>> GetAllSessionsAsync(string userId, ListSessionRequest request)
        {
            var entities = await this.sessionRepository.GetAll(userId, request).ConfigureAwait(false);

            return entities.Select(e => e.ToResponse()).ToList();
        }

        public async Task<DashboardStatistics> GetSessionStatistics(RequestContext requestContext, int year)
        {
            DashboardStatistics dashboardStatistics = await this.sessionRepository.GetDashboardStatsAsync(requestContext.UserId, requestContext.UserRole);
            IEnumerable<SessionStatistics> statistics = await this.sessionRepository.GetSessionStatistics(requestContext.UserId, year).ConfigureAwait(false);

            var allMonths = Enumerable.Range(1, 12)
            .GroupJoin(
                statistics,
                month => month,
                stat => stat.Month,
                (month, stats) => stats
                    .Select(stat => new SessionStatistics
                    {
                        Year = year,
                        Month = month,
                        TotalCost = stat.TotalCost,
                        SessionCount = stat.SessionCount
                    })
                    .DefaultIfEmpty(new SessionStatistics
                    {
                        Year = year,
                        Month = month,
                        TotalCost = 0,
                        SessionCount = 0
                    })
                    .Single()
            )
            .ToList();

            dashboardStatistics.SessionStatistics = allMonths;
            return dashboardStatistics;
        }
    }
}

﻿using Auth0.ManagementApi.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.App;
using sib_api_v3_sdk.Api;
using sib_api_v3_sdk.Client;
using sib_api_v3_sdk.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Task = System.Threading.Tasks.Task;

namespace PhotoGateway.BL.Services
{
    public class NotificationService : INotificationService
    {
        private readonly EmailConfiguration _emailConfig;
        private readonly ILogger<NotificationService> logger;
        private readonly Configuration _configuration = Configuration.Default;
        public NotificationService(ILogger<NotificationService> logger, IOptions<EmailConfiguration> emailConfig)
        {
            _emailConfig = emailConfig.Value;
            _configuration.ApiKey.Add("api-key", _emailConfig.ApiKey);
            this.logger = logger;
        }

        public string BuildEmailTemplate(string sessionID, string clientName, string photographerName, string sessionStatus, string sessionLink)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<!DOCTYPE html>");
            sb.AppendLine("<html>");
            sb.AppendLine("<head>");
            sb.AppendLine("  <meta charset=\"UTF-8\">");
            sb.AppendLine("  <title>Photo Session Notification</title>");
            sb.AppendLine("</head>");
            sb.AppendLine("<body>");
            sb.AppendLine("  <h1>Photo Session Update</h1>");
            sb.AppendLine("");
            sb.AppendLine("  <p>Dear customer,</p>");
            sb.AppendLine("");
            sb.AppendLine("  <p>We would like to inform you about an update regarding the photo session.</p>");
            sb.AppendLine("");
            sb.AppendLine("  <h2>Session Details:</h2>");
            sb.AppendLine("  <ul>");
            sb.AppendLine("    <li>Session ID: " + sessionID + "</li>");
            sb.AppendLine("    <li>Client: " + clientName + "</li>");
            sb.AppendLine("    <li>Photographer: " + photographerName + "</li>");
            sb.AppendLine("    <li>Status: " + sessionStatus + "</li>");
            sb.AppendLine("  </ul>");
            sb.AppendLine("");
            sb.AppendLine("  <p>To view the details and manage the session, please follow the link below:</p>");
            sb.AppendLine("  <p><a href=\"" + sessionLink + sessionID + "\">Session Details</a></p>");
            sb.AppendLine("");
            sb.AppendLine("  <p>Thank you for using our application!</p>");
            sb.AppendLine("");
            sb.AppendLine("  <p>Best regards,</p>");
            sb.AppendLine("  <p>PhotoGateway Team</p>");
            sb.AppendLine("</body>");
            sb.AppendLine("</html>");

            return sb.ToString();
        }


        public async Task NotifySessionUpdates(SessionWithUsersEntity session)
        {
            var client = session.Client.User;
            var photographer = session.Photographer.User;

            string templatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "EmailTemplates", "NewSessionTemplate.cshtml");
            var html = this.BuildEmailTemplate(session.Id, client.GetFullName(), photographer.GetFullName(), session.Status.ToString(), "http://localhost:5173/sessions/");

            List<SendSmtpEmailTo> users = new List<SendSmtpEmailTo>
            {
                 new SendSmtpEmailTo(client.Email, client.GetFullName()),
                 new SendSmtpEmailTo(photographer.Email, photographer.GetFullName())
            };

            await this.NotifyUsers(users, html).ConfigureAwait(false);
    }

        private async Task NotifyUsers(List<SendSmtpEmailTo> users, string message)
        {
            TransactionalEmailsApi apiInstance = new TransactionalEmailsApi(_configuration);
            SendSmtpEmail sendSmtpEmail = new SendSmtpEmail
            {
                To = users,
                Sender = new SendSmtpEmailSender(_emailConfig.FromName, _emailConfig.FromEmail),
                Subject = "PhotoGateway - Session Update",
                HtmlContent = message
            };

            try
            {
                CreateSmtpEmail result = await apiInstance.SendTransacEmailAsync(sendSmtpEmail);
            }
            catch (ApiException e)
            {
                this.logger.LogError($"Error sending email: {e.Message}");
            }
        }
    }
}

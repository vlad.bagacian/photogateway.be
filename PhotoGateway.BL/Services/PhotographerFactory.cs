﻿using PhotoGateway.BL.Extensions;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.Photographer.Response;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Services
{
    public class PhotographerFactory : IUserFactory
    {
        private readonly IPhotographerService photographerService;

        public PhotographerFactory(IPhotographerService photographerService)
        {
            this.photographerService = photographerService;
        }

        public async Task<UserBaseEntity> CreateUser(UserEntity userEntity)
        {
            PhotographerEntity photographer = userEntity.ToPhotographer();
            await this.photographerService.AddAsync(photographer);
            return photographer;
        }

        public async Task<string> GetId(UserEntity userEntity)
        {
            Photographer photographer = await this.photographerService.GetByIdAsync(userEntity.Id).ConfigureAwait(false);
            return photographer.Id;
        }
    }
}

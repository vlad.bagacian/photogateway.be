﻿using PhotoGateway.BL.Extensions;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.DAL.Models;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Services
{
    public class ClientFactory : IUserFactory
    {

        private readonly IClientService _clientService;

        public ClientFactory(IClientService clientService)
        {
            _clientService = clientService;
        }

        public async Task<UserBaseEntity> CreateUser(UserEntity userEntity)
        {
            ClientEntity client = userEntity.ToClient();
            await _clientService.AddAsync(client);
            return client;
        }

        public async Task<string> GetId(UserEntity userEntity)
        {
            ClientEntity client = await this._clientService.GetByIdAsync(userEntity.Id).ConfigureAwait(false);
            return client.Id;
        }
    }
}

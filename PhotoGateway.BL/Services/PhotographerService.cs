﻿using Azure.Core;
using PhotoGateway.BL.Extensions;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.DAL.Interfaces;
using PhotoGateway.DAL.Models;
using PhotoGateway.Domain;
using PhotoGateway.Domain.App;
using PhotoGateway.Domain.Exceptions;
using PhotoGateway.Domain.Photographer;
using PhotoGateway.Domain.Photographer.Request;
using PhotoGateway.Domain.Photographer.Response;
using PhotoGateway.Domain.PhotoReview.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Services
{
    public class PhotographerService : IPhotographerService
    {
        private readonly IPhotographerRepository<PhotographerEntity> photographerRepository;
        private readonly IPhotographerReviewRepository<PhotographerReviewEntity> photographerReviewRepository;

        public PhotographerService(IPhotographerRepository<PhotographerEntity> photographerRepository, IPhotographerReviewRepository<PhotographerReviewEntity> photographerReviewRepository)
        {
            this.photographerRepository = photographerRepository;
            this.photographerReviewRepository = photographerReviewRepository;
        }

        public async Task AddAsync(PhotographerEntity photographer)
        {
            await photographerRepository.AddAsync(photographer);
        }

        public async Task<Photographer> GetByIdAsync(string id)
        {
            PhotographerExtendedEntity photographerEntity = await photographerRepository.GetByUserId(id);
            if (photographerEntity == null)
            {
                throw new NotFoundException("The photographer was not found", "NOT_FOUND");
            }

            return photographerEntity.ToPhotographer();
        }

        public async Task<PhotographerProfile> GetAccount(RequestContext requestContext)
        {
            PhotographerExtendedEntity photographerEntity = await photographerRepository.GetByUserId(requestContext.User.Id).ConfigureAwait(false);
            if (photographerEntity == null)
            {
                throw new NotFoundException("The photographer was not found", "NOT_FOUND");
            }

            return photographerEntity.ToProfile();
        }

        public async Task<PhotographerProfile> GetProfile(string id)
        {
            PhotographerExtendedEntity photographerEntity = await photographerRepository.GetByPhotographerId(id).ConfigureAwait(false);
            if (photographerEntity == null)
            {
                throw new NotFoundException("The photographer was not found", "NOT_FOUND");
            }
            return photographerEntity.ToProfile();
        }

        public async Task<PhotographerProfile> UpdateProfile(RequestContext requestContext, PhotographerUpdateRequest updateRequest)
        {
            PhotographerExtendedEntity photographerEntity = await this.photographerRepository.GetByUserId(requestContext.User.Id).ConfigureAwait(true);
            if (photographerEntity == null)
            {
                throw new NotFoundException("The photographer was not found", "NOT_FOUND");
            }

            photographerEntity.Biography = updateRequest.Biography;
            photographerEntity.Skills = updateRequest.Skills;
            photographerEntity.Cost = updateRequest.Cost;
            photographerEntity.InstagramProfile = updateRequest.InstagramProfile;
            photographerEntity.FacebookProfile = updateRequest.FacebookProfile;
            if (!photographerEntity.IsProfileCompleted && photographerEntity.PortfolioPhotos != null && photographerEntity.PortfolioPhotos.Any())
            {
                photographerEntity.IsProfileCompleted = true;
            }

            await photographerRepository.UpdateAsync(photographerEntity.ToEntity());

            return photographerEntity.ToProfile();
        }

        public async Task<Photographer> UpdatePortfolio(RequestContext requestContext, List<PhotoMetadataEntity> portfolioPhotos)
        {
            PhotographerExtendedEntity photographerEntity = await this.photographerRepository.GetByUserId(requestContext.User.Id).ConfigureAwait(true);
            if (photographerEntity == null)
            {
                throw new NotFoundException("The photographer was not found", "NOT_FOUND");
            }

            photographerEntity.PortfolioPhotos = portfolioPhotos;

            await photographerRepository.UpdateAsync(photographerEntity.ToEntity());

            return photographerEntity.ToPhotographer();
        }

        public async Task<List<PhotographerResponse>> GetPhotographersAsync(ListPhotographersRequest request)
        {
            var result = await this.photographerRepository.GetAllFilteredAsync(request);

            return result.Select(p => p.ToResponse()).ToList();
        }

        public async Task<List<PhotographerReviewResponse>> AddReview(RequestContext requestContext, string photographerId, PhotographerReviewRequest request) 
        {
            PhotographerReviewEntity entity = request.ToEntity(photographerId, requestContext.AccountId);
            if (entity == null)
            {
                throw new NotFoundException("The photographer was not found", "NOT_FOUND");
            }

            await this.photographerReviewRepository.AddAsync(entity).ConfigureAwait(false);

            List<PhotographerReviewEntityExtended> photographerReviews = await this.photographerReviewRepository.GetByPhotographerId(photographerId);

            PhotographerEntity photographerEntity = (await this.photographerRepository.GetByPhotographerId(photographerId).ConfigureAwait(true)).ToEntity();
            if (photographerEntity == null)
            {
                throw new NotFoundException("The photographer was not found", "NOT_FOUND");
            }

            photographerEntity.Rating = photographerReviews.Sum(p => p.Rating) / photographerReviews.Count;
            await photographerRepository.UpdateAsync(photographerEntity);

            return photographerReviews.Select(p => p.ToResponse()).ToList();
        }

        public async Task<List<PhotographerReviewResponse>> GetReviews(string photographerId)
        {
            List<PhotographerReviewEntityExtended> photographerReviews = await this.photographerReviewRepository.GetByPhotographerId(photographerId);
            return photographerReviews.Select(p => p.ToResponse()).ToList();
        }
    }
}
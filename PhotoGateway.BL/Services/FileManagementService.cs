﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.App;
using PhotoGateway.Domain.Photographer;
using SharpCompress.Archives;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Metadata.Profiles.Exif;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Services
{
    public class FileManagementService : IFileManagementService
    {
        private readonly ILogger<FileManagementService> logger;

        public FileManagementService(ILogger<FileManagementService> logger)
        {
            this.logger = logger;
        }

        private async Task<PhotographyEntity> UploadFileAsync(string userId, string fileName, Stream file)
        {
            var uniqueFileName = $"{userId}_{Guid.NewGuid()}{fileName}";
            var directoryPath = Path.Combine("wwwroot", "Files"); // Store files in the wwwroot/Files folder
            var filePath = Path.Combine(directoryPath, uniqueFileName);

            // Ensure the directory exists
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            // Save the file to disk
            using (var fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                await file.CopyToAsync(fileStream);
            }

            file.Seek(0, SeekOrigin.Begin);
            using var image = Image.Load(file);

            var photo = new PhotographyEntity
            {
                Id = ObjectId.GenerateNewId().ToString(),
                Metadata = new PhotoMetadataEntity
                {
                    ImageUrl = uniqueFileName,
                    Ratio = (decimal)image.Width / image.Height
                },
                ExposureSettings = new ExposureSettings()
            };


            var exifProfile = image.Metadata.ExifProfile;

            if (exifProfile != null)
            {
                photo.CameraMake = exifProfile.GetValue(ExifTag.Make)?.Value?.ToString();
                photo.CameraModel = exifProfile.GetValue(ExifTag.Model)?.Value?.ToString();
                string creationDate = exifProfile.GetValue(ExifTag.DateTimeOriginal)?.Value?.ToString();
                photo.CreatedAt = creationDate != null ? DateTime.ParseExact(creationDate, "yyyy:MM:dd HH:mm:ss", CultureInfo.InvariantCulture) : DateTime.UtcNow;
                photo.ExposureSettings.Aperture = exifProfile.GetValue(ExifTag.FNumber)?.Value.ToString();
                photo.ExposureSettings.ShutterSpeed = exifProfile.GetValue(ExifTag.ExposureTime)?.ToString();
                photo.ExposureSettings.Iso = exifProfile.GetValue(ExifTag.ISOSpeedRatings)?.Value?.FirstOrDefault().ToString();
            }

            return photo;
        }

        public async Task<string> UploadFileAsync(string userId, Stream file)
        {
            var uniqueFileName = $"{userId}_{Guid.NewGuid()}_avatar";
            var directoryPath = Path.Combine("wwwroot", "Files"); // Store files in the wwwroot/Files folder
            var filePath = Path.Combine(directoryPath, uniqueFileName);

            // Ensure the directory exists
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            // Save the file to disk
            using (var fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                await file.CopyToAsync(fileStream);
            }

            return uniqueFileName;
        }

        public async Task<List<PhotographyEntity>> UploadPhotosAsync(string userId, List<IFormFile> photos)
        {
            var files = new ConcurrentBag<PhotographyEntity>();

            List<Task> fileUploadTasks = photos.Select(async photo =>
            {
                try
                {
                    if (photo.ContentType.StartsWith("image/"))
                    {

                        var fileName = photo.FileName;
                        var fileStream = photo.OpenReadStream();

                        // Upload file to Azure Blob Storage
                        var result = await this.UploadFileAsync(userId, fileName, fileStream);

                        // Add URL of uploaded file to the list
                        files.Add(result);

                    }
                    else if (Path.GetExtension(photo.FileName).Equals(".zip") || Path.GetExtension(photo.FileName).Equals(".rar") || Path.GetExtension(photo.FileName).Equals(".7z"))
                    {

                        using var stream = photo.OpenReadStream();
                        var memoryStream = new MemoryStream();
                        await stream.CopyToAsync(memoryStream);

                        memoryStream.Seek(0, SeekOrigin.Begin);
                        using var archive = ArchiveFactory.Open(memoryStream);
                        foreach (var entry in archive.Entries)
                        {
                            // Skip directories
                            if (entry.IsDirectory) continue;
                            using var extractedFile = entry.OpenEntryStream();
                            // Upload each extracted file to Azure using you

                            var result = await this.UploadFileAsync(userId, entry.Key, extractedFile);
                            files.Add(result);
                            // Add URL of uploaded file to the list

                        }
                    }
                }
                catch (Exception ex)
                {
                    this.logger.LogError(ex, ex.Message);
                }

            }).ToList();


            await Task.WhenAll(fileUploadTasks);

            return files.ToList();
        }
    }
}

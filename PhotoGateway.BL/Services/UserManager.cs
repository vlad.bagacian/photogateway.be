﻿
using PhotoGateway.BL.Extensions;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.DAL.Interfaces;
using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.UserModel.Request;
using PhotoGateway.Domain.UserModel.Response;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Services
{
    public class UserManager : IUserManager
    {
        private readonly IAuth0ManagementService _auth0ManagementService;
        private readonly IUserRepository<UserEntity> _userRepository;
        private readonly IUserFactory _userFactory;

        public UserManager(IAuth0ManagementService auth0ManagementService, IUserRepository<UserEntity> userRepository, IUserFactory userFactory)
        {
            _auth0ManagementService = auth0ManagementService;
            _userRepository = userRepository;
            _userFactory = userFactory;
        }

        public async Task CreateUserAsync(UserRequest userRequest)
        {
            // Call the Auth0 Management API to create the user
            var result = await _auth0ManagementService.CreateUserAsync(userRequest);

            UserEntity userEntity = userRequest.ToUser(result.userId, result.pictureUrl);

            // Add the user to the MongoDB database
            await _userRepository.AddAsync(userEntity);

            await _userFactory.CreateUser(userEntity);
        }

        public async Task<UserResponse> GetUserByAuth0Id(string auth0Id)
        {
            UserEntity userEntity = await _userRepository.GetByAuth0IdAsync(auth0Id);
            string eid = await _userFactory.GetId(userEntity);
            return userEntity.ToUser(eid);
        }

        public async Task<List<UserResponse>> GetUsersByIds(HashSet<string> userIds)
        {
            var entities = await _userRepository.GetAllByIds(userIds);
            return entities.Select(e => e.ToResponse()).ToList();
        }

        public async Task<UserResponse> UpdateUserAsync(string userId, UpdateUserRequest updateUserRequest)
        {
            UserEntity userEntity = await _userRepository.GetById(userId);

            userEntity.FirstName = updateUserRequest.FirstName != null ? updateUserRequest.FirstName : userEntity.FirstName;
            userEntity.LastName = updateUserRequest.LastName != null ? updateUserRequest.LastName : userEntity.LastName;
            userEntity.Phone = updateUserRequest.Phone != null ? updateUserRequest.Phone : userEntity.Phone;
            userEntity.Address = updateUserRequest.Address != null ? updateUserRequest.Address : userEntity.Address;

            await _userRepository.UpdateAsync(userEntity).ConfigureAwait(false);

            return userEntity.ToResponse();


        }

        public async Task<UserResponse> UpdateUserAvatarAsync(string userId, string avatarUrl)
        {
            UserEntity userEntity = await _userRepository.GetById(userId);
            userEntity.ImageUrl = avatarUrl;

            await _userRepository.UpdateAsync(userEntity).ConfigureAwait(false);

            return userEntity.ToResponse();
        }
    }
}

﻿using Azure.Core;
using MongoDB.Bson;
using PhotoGateway.BL.Extensions;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.DAL.Interfaces;
using PhotoGateway.DAL.Models;
using PhotoGateway.Domain;
using PhotoGateway.Domain.App;
using PhotoGateway.Domain.Exceptions;
using PhotoGateway.Domain.Photography.Request;
using PhotoGateway.Domain.PhotoReview.Request;
using PhotoGateway.Domain.PhotoReview.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Services
{
    public class PhotographyService : IPhotographyService
    {
        private readonly IPhotographyRepository<PhotographyEntity> photographyRepository;
        private readonly IPhotoReviewRepository<ReviewEntity> photoReviewRepository;

        public PhotographyService(IPhotographyRepository<PhotographyEntity> photographyRepository, IPhotoReviewRepository<ReviewEntity> photoReviewRepository, IFileManagementService fileManagementService, IUserManager userManager)
        {
            this.photographyRepository = photographyRepository;
            this.photoReviewRepository = photoReviewRepository;
        }

        public async Task AddAsync(PhotographyEntity photography)
        {
            await photographyRepository.AddAsync(photography);
        }

        public async Task AddManyAsync(List<PhotographyEntity> photograpies)
        {
            await photographyRepository.AddManyAsync(photograpies);
        }

        public async Task<List<PhotographyResponse>> GetAllPhotosAsync(string sessionId,  ListPhotosRequest request)
        {
            var entities = await this.photographyRepository.GetAllAsync(sessionId, request).ConfigureAwait(false);
            return entities.Select(e => e.ToResponse()).ToList();
        }

        
        public async Task<PhotographyResponse> MarkAsSelected(string photoId)
        {
            var entity = await this.photographyRepository.GetById(photoId);
            if (entity == null)
            {
                throw new NotFoundException("The photography was not found", "NOT_FOUND");
            }

            entity.Selected = !entity.Selected;
            await this.photographyRepository.UpdateAsync(entity).ConfigureAwait(false);

            return entity.ToResponse();
        }

        public async Task<PhotographyResponse> GetPhotoAsync(string photoId)
        {
            var entity = await this.photographyRepository.GetById(photoId);
            if (entity == null)
            {
                throw new NotFoundException("The photography was not found", "NOT_FOUND");
            }

            return entity.ToResponse();
        }


        public async Task<List<PhotoReviewResponse>> GetReviewsAsync(RequestContext requestContext, string photoId, ListReviewsRequest request)
        {
            PhotographyEntity photo = await this.photographyRepository.GetById(photoId);
            if (photo == null)
            {
                throw new NotFoundException("The photography was not found", "NOT_FOUND");
            }

            List<ReviewExtendedEntity> photoReviews = await this.photoReviewRepository.GetByPhotoId(photoId, request);
            return photoReviews.Select(pr => pr.ToResponse(requestContext)).ToList();
        }

        public async Task<List<PhotoReviewResponse>> AddReviewAsync(RequestContext requestContext, string photoId, AddCommentRequest request)
        {
            PhotographyEntity photo = await this.photographyRepository.GetById(photoId);
            if (photo == null)
            {
                throw new NotFoundException("The photography was not found", "NOT_FOUND");
            }


            ReviewEntity entity = new ReviewEntity
            {
                Id = ObjectId.GenerateNewId().ToString(),
                PhotoId = photo.Id,
                UserId = requestContext.AccountId,
                Comment = request.Comment,
                CreatedAt = DateTime.UtcNow
            };

            await this.photoReviewRepository.AddAsync(entity).ConfigureAwait(false);

            List<ReviewExtendedEntity> photoReviews = await this.photoReviewRepository.GetByPhotoId(photoId, new ListReviewsRequest());

            return photoReviews.Select(pr => pr.ToResponse(requestContext)).ToList();
        }

        public async Task<List<PhotographyResponse>> GetAllPhotosInSession(string sessionId)
        {
            var entities = await this.photographyRepository.GetAllAsync(sessionId).ConfigureAwait(false);
            return entities.Select(e => e.ToResponse()).ToList();
        }

        public async Task<PhotographyResponse> UpdatePhotoAsync(string photoId, PhotographyEntity photographyEntity)
        {
            var entity = await this.photographyRepository.GetById(photoId);
            if (entity == null)
            {
                throw new NotFoundException("The photography was not found", "NOT_FOUND");
            }

            entity.ModifiedAt = photographyEntity.CreatedAt;
            if (entity.Metadata.History == null)
            {
                entity.Metadata.History = new List<string>();
            }
            entity.Metadata.History.Add(entity.Metadata.ImageUrl);
            entity.Metadata.ImageUrl = photographyEntity.Metadata.ImageUrl;

            await this.photographyRepository.UpdateAsync(entity);

            return entity.ToResponse();
        }
    }
}

﻿using PhotoGateway.BL.Interfaces;
using PhotoGateway.DAL.Models;
using PhotoGateway.Domain.UserModel;
using System;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Services
{
    public class UserFactory : IUserFactory
    {
        private readonly IPhotographerService photographerService;
        private readonly IClientService clientService;

        public UserFactory(IPhotographerService photographerService, IClientService clientService)
        {
            this.photographerService = photographerService;
            this.clientService = clientService;
        }

        public async Task<UserBaseEntity> CreateUser(UserEntity userEntity)
        {
            IUserFactory factory = GetFactory(userEntity);
            return await factory.CreateUser(userEntity).ConfigureAwait(false);
        }

        public async Task<string> GetId(UserEntity userEntity)
        {
            IUserFactory factory = GetFactory(userEntity);
            return await factory.GetId(userEntity).ConfigureAwait(false);
        }

        private IUserFactory GetFactory(UserEntity user)
        {
            switch (user.Role)
            {
                case UserRole.Photographer:
                    return new PhotographerFactory(photographerService);

                case UserRole.Client:
                    return new ClientFactory(clientService);

                default:
                    throw new ArgumentException($"Invalid user role: {user.Role}");
            }
        }
    }
}

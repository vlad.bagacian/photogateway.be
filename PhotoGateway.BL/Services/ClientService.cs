﻿using PhotoGateway.BL.Interfaces;
using PhotoGateway.DAL.Interfaces;
using PhotoGateway.DAL.Models;
using System.Threading.Tasks;

namespace PhotoGateway.BL.Services
{
    public class ClientService : IClientService
    {
        private readonly IClientRepository<ClientEntity> clientRepository;

        public ClientService(IClientRepository<ClientEntity> clientRepository)
        {
            this.clientRepository = clientRepository;
        }

        public async Task AddAsync(ClientEntity client)
        {
            await clientRepository.AddAsync(client);
        }

        public async Task<ClientEntity> GetByIdAsync(string id)
        {
            ClientEntity clientEntity = await clientRepository.GetByUserId(id);
            return clientEntity;
        }

        public async Task UpdateAsync(ClientEntity client)
        {
            await clientRepository.UpdateAsync(client);
        }
    }
}

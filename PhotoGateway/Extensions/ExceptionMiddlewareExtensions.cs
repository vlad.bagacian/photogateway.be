﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using PhotoGateway.Domain.Exceptions;
using System;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace PhotoGateway.Extensions
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();
            //app.UseExceptionHandler(appError =>
            //{
            //    appError.Run(context =>
            //    {
            //        context.Response.ContentType = "application/json";
            //        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            //        var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
            //        //if (contextFeature != null && contextFeature.Error is ServiceException)
            //        //{ 
            //        //    var exception = contextFeature.Error as ServiceException;
            //        //    context.Response.StatusCode = (int)exception.StatusCode;
            //        //    var errorDetails = new ErrorDetails()
            //        //    {
            //        //        StatusCode = (int)exception.StatusCode,
            //        //        Message = exception.Message,
            //        //        ErrorCode = exception.ErrorCode
            //        //    };
            //        //    var jsonErrorDetails = JsonSerializer.Serialize(errorDetails);
            //        //    return context.Response.WriteAsJsonAsync(jsonErrorDetails);
            //        //}
            //        return context.Response.WriteAsJsonAsync("INTERNAL_ERROR");
            //    });
            //});
        }

        
    }

    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                // Handle the exception and return a custom error response
                await HandleExceptionAsync(context, ex);
            }
        }
        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            // Log the exception, perform any additional actions, etc.

            // Set the response status code and content type
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            context.Response.ContentType = "application/json";
            ErrorDetails errorDetails = new ErrorDetails
            {
                Message = "An error occurred.",
                StatusCode = (int)HttpStatusCode.InternalServerError,
                ErrorCode = "INTERNAL_SERVER_ERROR"
            };

            if (exception is ServiceException)
            {
                var servEx = exception as ServiceException;
                context.Response.StatusCode = (int)servEx.StatusCode;
                errorDetails = new ErrorDetails()
                {
                    StatusCode = (int)servEx.StatusCode,
                    Message = servEx.Message,
                    ErrorCode = servEx.ErrorCode
                };

            }

            // Serialize the response object to JSON
            var jsonResponse = JsonSerializer.Serialize(errorDetails);

            // Write the JSON response to the response body
            return context.Response.WriteAsync(jsonResponse);
        }
    }
}

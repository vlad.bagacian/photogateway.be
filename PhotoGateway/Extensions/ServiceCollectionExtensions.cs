﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.BL.Services;
using PhotoGateway.DAL;
using PhotoGateway.DAL.Interfaces;
using PhotoGateway.DAL.Repositories;
using PhotoGateway.Domain.App;
using System;
using System.Linq;
using System.Reflection;
using System.Security.Claims;

namespace PhotoGateway.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddAppSettings(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<Auth0Settings>(configuration.GetSection(nameof(Auth0Settings)));
            services.Configure<MongoDbSettings>(configuration.GetSection(nameof(MongoDbSettings)));
            services.Configure<AzureSettings>(configuration.GetSection(nameof(AzureSettings)));
            services.Configure<EmailConfiguration>(configuration.GetSection(nameof(EmailConfiguration)));
        }

        public static void AddAuthenticationAndAuthorization(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Authority = $"https://{configuration[Auth0Configuration.ClientDomain]}/";
                    options.Audience = $"https://{configuration[Auth0Configuration.Audience]}";
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        NameClaimType = ClaimTypes.NameIdentifier
                    };

                });
        }

        public static void AddServices(this IServiceCollection services)
        {
            services.AddRazorPages();
            var assembly = Assembly.GetAssembly(typeof(IService));

            var classes = assembly.GetTypes()
                .Where(type => type.IsClass && typeof(IService).IsAssignableFrom(type))
                .ToList();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<RequestContext>();

            foreach (var type in classes)
            {
                Type typeInterface = type.GetInterfaces().FirstOrDefault();
                if (typeInterface != null)
                {
                    services.AddSingleton(typeInterface, type);
                }
            }
        }

        public static void AddRepositories(this IServiceCollection services)
        {
            var repositoryType = typeof(MongoRepository<>);
            var assembly = Assembly.GetAssembly(repositoryType);

            var repositories = assembly.GetTypes()
                .Where(type => type.IsClass && !type.IsAbstract && type.GetInterfaces()
                .Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IRepository<>)))
                .ToList();

            foreach (var repository in repositories)
            {
                Type typeInterface = repository.GetInterfaces().FirstOrDefault();
                if (typeInterface != null)
                {
                    services.AddSingleton(typeInterface, repository);
                }
            }
        }
    }
}


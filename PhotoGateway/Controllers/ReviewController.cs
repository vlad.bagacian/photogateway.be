﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.Domain;
using PhotoGateway.Domain.App;
using PhotoGateway.Domain.Photography.Request;
using PhotoGateway.Domain.PhotoReview.Request;
using PhotoGateway.Domain.PhotoReview.Response;
using System.Threading.Tasks;

namespace PhotoGateway.Controllers
{
    [Route("api/sessions/{sessionId}/photographies/{photoId}/reviews")]
    [ApiController]
    public class ReviewController : ControllerBase
    {
        private readonly ILogger<ReviewController> logger;
        private ISessionService sessionService;
        private IPhotographyService photographyService;
        private RequestContext requestContext;

        public ReviewController(ILogger<ReviewController> logger, RequestContext requestContext, ISessionService sessionService, IPhotographyService photographyService)
        {
            this.logger = logger;
            this.requestContext = requestContext;
            this.sessionService = sessionService;
            this.photographyService = photographyService;
        }

        [HttpPost]
        public async Task<IActionResult> AddCommentAsync(string sessionId, string photoId, [FromBody] AddCommentRequest request)
        {
            var session = await this.sessionService.GetSessionAsync(this.requestContext.UserId, sessionId).ConfigureAwait(true);
            if (session == null)
            {
                // bai mare
            }

            var response = await this.photographyService.AddReviewAsync(this.requestContext, photoId, request).ConfigureAwait(true);
            return Ok(new ListResponse<PhotoReviewResponse>(response));
        }

        [HttpGet]
        public async Task<IActionResult> GetAllReviews(string sessionId, string photoId, [FromQuery] ListReviewsRequest request)
        {
            var session = await this.sessionService.GetSessionAsync(this.requestContext.UserId, sessionId).ConfigureAwait(true);
            if (session == null)
            {
                // bai mare
            }

            var response = await this.photographyService.GetReviewsAsync(this.requestContext, photoId, request).ConfigureAwait(true);

            return Ok(new ListResponse<PhotoReviewResponse>(response));
        }
    }
}

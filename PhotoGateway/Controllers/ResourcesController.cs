﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace PhotoGateway.Controllers
{
    [Route("api/resources")]
    [ApiController]
    public class ResourcesController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public ResourcesController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet("locations")]
        public ActionResult<string[]> GetLocations()
        {
            var locations = _configuration.GetSection("Locations").Get<string[]>();

            return Ok(locations);
        }
    }
}

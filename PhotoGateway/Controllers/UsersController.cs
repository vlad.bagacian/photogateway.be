﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoGateway.BL.Extensions;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.Domain.App;
using PhotoGateway.Domain.UserModel.Request;
using System.Threading.Tasks;

namespace PhotoGateway.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        private readonly IUserManager userManager;
        private readonly RequestContext requestContext;
        private readonly IFileManagementService fileManagementService;
        private IHttpContextAccessor httpContextAccessor;
        public UsersController(IUserManager userManager, RequestContext requestContext, IFileManagementService fileManagementService, IHttpContextAccessor httpContextAccessor)
        {
            this.userManager = userManager;
            this.requestContext = requestContext;
            this.fileManagementService = fileManagementService;
            this.httpContextAccessor = httpContextAccessor;
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody] UserRequest userCreateRequest)
        {
            await userManager.CreateUserAsync(userCreateRequest);
            return Ok();
        }

        [HttpPatch]
        public async Task<IActionResult> UpdateUser([FromBody] UpdateUserRequest updateUserRequest)
        {
            var result = await userManager.UpdateUserAsync(this.requestContext.AccountId, updateUserRequest);
            return Ok(result);
        }

        [Authorize]
        [HttpPost("update-avatar")]
        public async Task<IActionResult> UpdateAvatar([FromForm(Name = "avatar")] IFormFile avatar)
        {
            var fileStream = avatar.OpenReadStream();
            var avatarUrl = await this.fileManagementService.UploadFileAsync(this.requestContext.UserId, fileStream).ConfigureAwait(true);
            avatarUrl = $"{GetWebAddress()}/api/files/{avatarUrl}";
            var userProfile = await userManager.UpdateUserAvatarAsync(this.requestContext.AccountId, avatarUrl);
            return Ok(userProfile);
        }

        [HttpGet]
        [Authorize]
        public IActionResult GetProfile()
        {
            var user = requestContext.User.ToProfile();
            return Ok(user);
        }

        private string GetWebAddress() => $"{httpContextAccessor.HttpContext.Request.Scheme}://{httpContextAccessor.HttpContext.Request.Host}{httpContextAccessor.HttpContext.Request.PathBase}";
    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.Domain.App;
using SharpCompress.Archives;
using SharpCompress.Archives.Zip;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.Processing;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoGateway.Controllers
{
    [Route("api/files")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        private readonly RequestContext requestContext;
        private readonly IFileManagementService fileManagementService;
        private readonly ILogger<FilesController> logger;

        public FilesController(ILogger<FilesController> logger, RequestContext requestContext, IFileManagementService fileManagementService)
        {
            this.requestContext = requestContext;
            this.fileManagementService = fileManagementService;
            this.logger = logger;
        }

        [HttpGet("{fileName}")]
        public async Task<IActionResult> Get(string fileName, [FromQuery] int? w, [FromQuery] int q = 100)
        {
            var directoryPath = Path.Combine("wwwroot", "Files");
            var filePath = Path.Combine(directoryPath, fileName);
            
            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, true))
            {
                var image = await Image.LoadAsync(stream);
                image.Mutate(x => x.Resize(new ResizeOptions
                {
                    Size = new Size(w ?? image.Width, 0),
                    Mode = ResizeMode.Max
                }));

                var outputStream = new MemoryStream();
                image.Save(outputStream, new JpegEncoder
                {
                    Quality = q
                });

                outputStream.Seek(0, SeekOrigin.Begin);

                // Set the response headers
                Response.ContentType = "image/jpeg";
                Response.ContentLength = outputStream.Length;

                // Write the response content using PushContent
                await Response.StartAsync();
                await Response.BodyWriter.WriteAsync(outputStream.ToArray());
                await Response.CompleteAsync();

                return new EmptyResult();
            }
        }
    }
}

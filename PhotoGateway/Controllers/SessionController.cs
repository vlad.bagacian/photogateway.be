﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.BL.Services;
using PhotoGateway.Domain;
using PhotoGateway.Domain.App;
using PhotoGateway.Domain.Session;
using PhotoGateway.Domain.Session.Request;
using PhotoGateway.Domain.Session.Response;
using SharpCompress.Archives;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoGateway.Controllers
{
    [Route("api/sessions")]
    [ApiController]
    [Authorize]
    public class SessionController : ControllerBase
    {

        private ISessionService sessionService;
        private RequestContext requestContext;

        public SessionController(ILogger<SessionController> logger, RequestContext requestContext, ISessionService sessionService)
        {
            this.sessionService = sessionService;
            this.requestContext = requestContext;
        }

        [HttpPost]
        public async Task<IActionResult> CreateSessionAsync([FromBody] CreateSessionRequest request)
        {
            var response = await this.sessionService.CreateSessionAsync(this.requestContext.UserId, request).ConfigureAwait(true);
            return Ok(response);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllSessions([FromQuery] ListSessionRequest request)
        {
            var response = await this.sessionService.GetAllSessionsAsync(this.requestContext.UserId, request).ConfigureAwait(true);
            return Ok(new ListResponse<SessionResponse>(response));
        }

        [HttpGet("statistics")]
        public async Task<IActionResult> GetStatistics([FromQuery] int year = 2023)
        {
            var response = await this.sessionService.GetSessionStatistics(this.requestContext, year).ConfigureAwait(true);
            return Ok(response);
        }

        [HttpGet("{sessionId}")]
        public async Task<IActionResult> GetSessionAsync(string sessionId)
        {
            var response = await this.sessionService.GetSessionAsync(this.requestContext.UserId, sessionId).ConfigureAwait(true);
            return Ok(response);
        }

        [HttpPut("{sessionId}")]
        public async Task<IActionResult> UpdateSessionAsync(string sessionId, [FromBody] UpdateSessionRequest request)
        {
            var response = await this.sessionService.UpdateSessionAsync(this.requestContext.UserId, sessionId, request).ConfigureAwait(true);
            return Ok(response);
        }

        [HttpPost("{sessionId}/next")]
        public async Task<IActionResult> MoveNext(string sessionId)
        {
            var response = await this.sessionService.MoveNext(this.requestContext.UserId, sessionId).ConfigureAwait(true);  
            return Ok(response);
        }

        [HttpPost("{sessionId}/back")]
        public async Task<IActionResult> MoveBack(string sessionId)
        {
            var response = await this.sessionService.MoveBack(this.requestContext.UserId, sessionId).ConfigureAwait(true);
            return Ok(response);
        }
    }
}

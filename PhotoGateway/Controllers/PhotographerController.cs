﻿using Azure.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PhotoGateway.BL.Extensions;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.Domain;
using PhotoGateway.Domain.App;
using PhotoGateway.Domain.Photographer;
using PhotoGateway.Domain.Photographer.Request;
using PhotoGateway.Domain.Photographer.Response;
using SharpCompress.Archives;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace PhotoGateway.Controllers
{
    [Route("api/photographers")]
    public class PhotographerController : ControllerBase
    {
        private readonly RequestContext requestContext;
        private readonly IPhotographerService photographerService;
        private readonly IFileManagementService fileManagementService;
        private readonly ILogger<PhotographerController> logger;

        public PhotographerController(ILogger<PhotographerController> logger, 
            RequestContext requestContext, 
            IPhotographerService photographerService, 
            IPhotographyService photographyService,
            IFileManagementService fileManagementService
            )
        {
            this.requestContext = requestContext;
            this.photographerService = photographerService;
            this.fileManagementService = fileManagementService;
            this.logger = logger;
        }


        [Authorize]
        [HttpPost("update-profile")]
        public async Task<IActionResult> UpdateProfile([FromBody] PhotographerUpdateRequest photographerUpdateRequest)
        {
            PhotographerProfile photographer = await this.photographerService.UpdateProfile(this.requestContext, photographerUpdateRequest).ConfigureAwait(true);
            return Ok(photographer);
        }

        [Authorize]
        [HttpPost("update-portfolio")]
        public async Task<IActionResult> UploadFiles([FromForm(Name = "files")] List<IFormFile> files)
        {
            if (!files.Any())
            {
                return BadRequest();
            }

            var result = await this.fileManagementService.UploadPhotosAsync(this.requestContext.UserId, files).ConfigureAwait(true);
            var photos = result.Select(r => r.Metadata).ToList();
            await this.photographerService.UpdatePortfolio(requestContext, photos).ConfigureAwait(true);

            return Ok();
        }

        [Authorize]
        [HttpGet("myaccount")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PhotographerProfile))]
        public async Task<IActionResult> GetProfile()
        {
            PhotographerProfile photographer = await this.photographerService.GetAccount(this.requestContext).ConfigureAwait(true);
            return Ok(photographer);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PhotographerProfile))]
        public async Task<IActionResult> GetProfile(string id)
        {
            PhotographerProfile photographer = await this.photographerService.GetProfile(id).ConfigureAwait(true);
            return Ok(photographer);
        }

        [HttpPost("search")]
        public async Task<IActionResult> SearchPhotographersAsync([FromBody] ListPhotographersRequest request)
        {
            var photographers = await this.photographerService.GetPhotographersAsync(request).ConfigureAwait(true);
           
            var response = new ListResponse<PhotographerResponse>(photographers);
            return Ok(response);
        }

        [HttpPost("{photographerId}/reviews")]
        public async Task<IActionResult> AddReviewAsync(string photographerId, [FromBody] PhotographerReviewRequest request)
        {
            if (this.requestContext.User.Role == Domain.UserModel.UserRole.Photographer)
            {
                return BadRequest();
            }

            var result = await this.photographerService.AddReview(this.requestContext, photographerId, request).ConfigureAwait(true);
            var response = new ListResponse<PhotographerReviewResponse>(result);
            return Ok(response);
        }

        [HttpGet("{photographerId}/reviews")]
        public async Task<IActionResult> GetReviewsAsync(string photographerId)
        {
            var result = await this.photographerService.GetReviews(photographerId).ConfigureAwait(true);
            var response = new ListResponse<PhotographerReviewResponse>(result);
            return Ok(response);
        }
    }
}

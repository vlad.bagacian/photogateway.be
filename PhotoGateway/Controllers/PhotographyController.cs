﻿using Azure.Core;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.Domain;
using PhotoGateway.Domain.App;
using PhotoGateway.Domain.Photography.Request;
using SharpCompress.Common.Zip;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.Processing;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipelines;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static System.Collections.Specialized.BitVector32;
using ZipEntry = ICSharpCode.SharpZipLib.Zip.ZipEntry;

namespace PhotoGateway.Controllers
{
    [Route("api/sessions/{sessionId}/photographies")]
    [ApiController]
    [Authorize]
    public class PhotographyController : ControllerBase
    {
        private readonly ILogger<PhotographyController> logger;
        private IPhotographyService photographyService;
        private ISessionService sessionService;
        private IFileManagementService fileManagementService;
        private RequestContext requestContext;

        public PhotographyController(ILogger<PhotographyController> logger, RequestContext requestContext, IPhotographyService photographyService, IFileManagementService fileManagementService, ISessionService sessionService)
        {
            this.logger = logger;
            this.photographyService = photographyService;
            this.requestContext = requestContext;
            this.fileManagementService = fileManagementService;
            this.sessionService = sessionService;
        }

        [HttpPost]
        public async Task<IActionResult> AddPhotoAsync(string sessionId, [FromForm(Name = "files")] List<IFormFile> files)
        {
            var result = await this.fileManagementService.UploadPhotosAsync(this.requestContext.UserId, files).ConfigureAwait(true);
            result.ForEach(r => r.SessionId = sessionId);
            await this.photographyService.AddManyAsync(result).ConfigureAwait(true);

            return NoContent();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPhotosAsync(string sessionId, [FromQuery] ListPhotosRequest request)
        {
            var session = await this.sessionService.GetSessionAsync(this.requestContext.UserId, sessionId).ConfigureAwait(true);
            if (session == null)
            {
                // bai mare
            }

            var response = await this.photographyService.GetAllPhotosAsync(session.Id, request).ConfigureAwait(true);

            return Ok(new ListResponse<PhotographyResponse>(response));
        }

        [HttpGet("zip")]
        public async Task<IActionResult> ZipFiles(string sessionId)
        {
            var photos = await this.photographyService.GetAllPhotosInSession(sessionId).ConfigureAwait(true);

            // Set the content type, content disposition, and buffer output
            Response.ContentType = "application/zip";
            Response.Headers.Add("Content-Disposition", "attachment; filename=download.zip");

            // Create a ZipOutputStream to the response stream
            using var zipStream = new ZipOutputStream(Response.BodyWriter.AsStream());

            foreach (var fileName in photos.Where(p => p.Selected).Select(p => p.Metadata.ImageUrl))
            {
                var directoryPath = Path.Combine("wwwroot", "Files");
                var filePath = Path.Combine(directoryPath, fileName);

                // Create a new entry for the file in the zip archive
                var entry = new ZipEntry(Path.GetFileName(filePath));
                zipStream.PutNextEntry(entry);

                // Stream the file content to the zip entry
                using var fileStream = System.IO.File.OpenRead(filePath);
                await fileStream.CopyToAsync(zipStream);

                // Close the entry to prevent memory leaks
                zipStream.CloseEntry();
            }

            // Close the zip stream and flush the response
            await zipStream.FlushAsync();

            return new EmptyResult();

        }

        [HttpGet("{photoId}")]
        public async Task<IActionResult> GetPhotoAsync(string sessionId, string photoId)
        {
            var session = await this.sessionService.GetSessionAsync(this.requestContext.UserId, sessionId).ConfigureAwait(true);
            if (session == null)
            {
                // bai mare
            }

            var response = await this.photographyService.GetPhotoAsync(photoId).ConfigureAwait(true);
            return Ok(response);
        }

        [HttpPut("{photoId}")]
        public async Task<IActionResult> UpdatePhotoAsync(string sessionId, string photoId, [FromForm(Name = "files")] List<IFormFile> files)
        {
            var session = await this.sessionService.GetSessionAsync(this.requestContext.UserId, sessionId).ConfigureAwait(true);
            if (session == null)
            {
                // bai mare
            }

            var result = await this.fileManagementService.UploadPhotosAsync(this.requestContext.UserId, files).ConfigureAwait(true);

            var response = await this.photographyService.UpdatePhotoAsync(photoId, result.FirstOrDefault()).ConfigureAwait(true);
            return Ok(response);
        }

        [HttpPut("{photoId}/mark-selected")]
        public async Task<IActionResult> MarkPhotosAsSelected(string sessionId, string photoId)
        {
            var session = await this.sessionService.GetSessionAsync(this.requestContext.UserId, sessionId).ConfigureAwait(true);
            if (session == null)
            {
                // bai mare
            }

            var response = await this.photographyService.MarkAsSelected(photoId).ConfigureAwait(true);
            return Ok(response);
        }

    }
}

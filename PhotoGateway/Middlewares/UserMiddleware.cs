﻿using Amazon.Runtime.Internal.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using PhotoGateway.BL.Interfaces;
using PhotoGateway.Domain.UserModel.Response;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PhotoGateway.Middlewares
{
    public class UserMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IUserManager _userManager;
        private readonly ILogger<UserMiddleware> logger;

        public UserMiddleware(RequestDelegate next, IUserManager userManager, ILogger<UserMiddleware> logger)
        {
            _next = next;
            _userManager = userManager;
            this.logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (context.User.Identity.IsAuthenticated)
            {
                var auth0Id = context.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                UserResponse user = await _userManager.GetUserByAuth0Id(auth0Id);
                if (user == null)
                {
                    context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                    await context.Response.WriteAsync("Unauthorized");
                    return;
                }

                // Retrieve the "email_verified" claim from the token
                string emailVerified = context.User.FindFirst("email_verified")?.Value;

                // Convert the string value to a boolean
                bool.TryParse(emailVerified, out bool isEmailVerified);

                // Update the AppUser object with the email verification status
                user.EmailVerified = isEmailVerified;

                // Add the AppUser object to the request context for future use
                context.Items["User"] = user;
            }

            await _next(context);
        }
    }
}
